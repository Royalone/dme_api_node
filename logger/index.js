var moment = require('moment')
var colorConsole = require('tracer').colorConsole({
	format: [
		'{{timestamp}} ({{file}}:{{line}}) >> {{message}}', // default format
		{
			error: '{{timestamp}} <{{title}}> {{message}} ({{file}}:{{line}})\nCall Stack:\n{{stack}}' // error format
		}
	],
	dateformat: 'HH:MM:ss.L',
	preprocess: function (data) {
		data.title = data.title.toUpperCase()
	}
})

module.exports = {
	info: (message) => {
		message.timestamp = moment().format('YYYY-MM-DDTHH:mm:ss')
		colorConsole.info(message)
		global.logFile.info(message)
	}
}
