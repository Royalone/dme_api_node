require('module-alias/register')
require('dotenv-flow').config()

var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var morgan = require('morgan')
var bodyParser = require('body-parser')

var indexRouter = require('./routes/index')

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.use(bodyParser.json({ limit: '5mb' }))
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use('/', indexRouter)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message
	res.locals.error = req.app.get('env') === 'development' ? err : {}

	// render the error page
	res.status(err.status || 500)
	res.render('DME Node layer')
})

var winston = require('winston')

var options = {
	file: {
		level: 'info',
		filename: 'logs/app.log',
		handleExceptions: true,
		maxsize: 5242880, // 5MB
		maxFiles: 5,
		colorize: false,
		format: winston.format.combine(
			winston.format.timestamp(),
			winston.format.printf((info) => {
				return `[DME LOG] [${info.timestamp}] ### ${info.message}`;
			})
     	)
	}
}

// instantiate a new Winston Logger with the settings defined above
var logFile = winston.createLogger({
	transports: [
		new winston.transports.File(options.file),
		// new winston.transports.Console(options.console)
	],
	exitOnError: false // do not exit on handled exceptions
})

// create a stream object with a 'write' function that will be used by `morgan`
logFile.stream = {
	write: function (message, encoding) {
		// use the 'info' log level so the output will be picked up by both transports (file and console)
		logFile.info(message)
	}
}

global.logFile = logFile

app.use(morgan('combined', { stream: logFile.stream }))
module.exports = app