var express = require('express')
var router = express.Router()
var consts = require('consts')
var booking = require('controller/booking')
var labelling = require('controller/labelling')
var order = require('controller/order')
var pod = require('controller/pod')
var pricing = require('controller/pricing')
var tracking = require('controller/tracking')
var servicecode = require('controller/servicecode')

/* GET home page. */
router.get('/', function (req, res, next) {
	res.render('index', { title: 'Express' })
})

router.post(`${consts.BOOKING_BASE_PATH}${consts.BOOKING_CONSIGNMENT_PATH}`, booking.bookConsignment)
router.delete(`${consts.BOOKING_BASE_PATH}${consts.CANCEL_BOOKING_CONSIGNMENT_PATH}`, booking.cancelConsignment)
router.put(`${consts.BOOKING_BASE_PATH}${consts.UPDATE_BOOKING_CONSIGNMENT_PATH}`, booking.updateConsignment)
router.post(`${consts.BOOKING_BASE_PATH}${consts.RE_BOOKING_CONSIGNMENT_PATH}`, booking.rebookConsignment)
router.post(`${consts.LABELS_BASE_PATH}${consts.GET_LABEL_PATH}`, labelling.getConsignmentLabel)
router.post(`${consts.LABELS_BASE_PATH}${consts.REPRINT_LABEL_PATH}`, labelling.reprintLabel)
router.post(`${consts.LABELS_BASE_PATH}${consts.CREATE_LABEL_PATH}`, labelling.createConsignmentLabel)
router.post(`${consts.ORDER_BASE_PATH}${consts.ORDER_SUMMARY_PATH}`, order.getOrderSummary)
router.post(`${consts.ORDER_BASE_PATH}${consts.ORDER_CREATE_PATH}`, order.createOrder)
router.post(`${consts.POD_BASE_PATH}${consts.GET_POD_PATH}`, pod.fetchPod)
// router.post(`${consts.POD_BASE_PATH}${consts.UPLOAD_POD_PATH}`, pod.uploadPod)
router.post(`${consts.PRICING_BASE_PATH}${consts.CALCULATE_PRICE_PATH}`, pricing.calculatePriceForConsignment)
router.post(`${consts.PRICING_BASE_PATH}${consts.GET_ETD_PATH}`, pricing.getEtd)
router.post(`${consts.TRACKING_BASE_PATH}${consts.TRACK_ALL_DETAILS_PATH}`, tracking.trackAllDetails)
router.post(`${consts.SERVICECODE_BASE_PATH}${consts.GETACCOUNTS_BASE_PATH}`, servicecode.getAccounts)

module.exports = router
