module.exports = {
	BOOKING_BASE_PATH: '/booking',
	UPDATE_BOOKING_CONSIGNMENT_PATH: '/updateconsignment',
	CANCEL_BOOKING_CONSIGNMENT_PATH: '/cancelconsignment',
	RE_BOOKING_CONSIGNMENT_PATH: '/rebookconsignment',
	BOOKING_CONSIGNMENT_PATH: '/bookconsignment',
	VALIDATE_BOOKING_PATH: '/validatebooking',
	DISPATCH_BOOKING_PATH: '/dispatchjobs',
	CANCEL_PENDING_JOB_PATH: '/cancelpendingjobs',
	CANCEL_DISPATCH_JOB_PATH: '/canceldispatchjobs',
	GET_PENDING_JOB_PATH: '/getpendingjobs',

	PRICING_BASE_PATH: '/pricing',
	CALCULATE_PRICE_PATH: '/calculateprice',
	GET_ETD_PATH: '/getetd',

	LABELS_BASE_PATH: '/labelling',
	GET_LABEL_PATH: '/getlabel',
	CREATE_LABEL_PATH: '/createlabel',
	REPRINT_LABEL_PATH: '/reprint',

	TRACKING_BASE_PATH: '/tracking',
	TRACK_ALL_DETAILS_PATH: '/trackconsignment',

	ORDER_BASE_PATH: '/order',
	ORDER_SUMMARY_PATH: '/summary',
	ORDER_CREATE_PATH: '/create',
	POD_BASE_PATH: '/pod',
	GET_POD_PATH: '/fetchpod',
	UPLOAD_POD_PATH: '/uploadpod',

	SERVICECODE_BASE_PATH: '/servicecode',
	GETACCOUNTS_BASE_PATH: '/getaccounts',

	SERVICE_PROVIDER: [
		'ALLIED',
		'BEX',
		'CAPITAL',
		'HUNTER',
		'HUNTER_V2',
		'ST',
		'AUSPOST',
		'TNT',
		'DHL',
		'FASTWAY',
		'DPD',
		'SENDLE'
	],

	SERVICE_FP_ENABLE: {
		ALLIED: true,
		BEX: true,
		CAPITAL: true,
		HUNTER: true,
		HUNTER_V2: true,
		ST: true,
		AUSPOST: true,
		TNT: true,
		DHL: true,
		FASTWAY: true,
		SENDLE: true
	}
}

String.prototype.replaceAll = function (search, replacement) {
	var target = this
	return target.split(search).join(replacement)
}
