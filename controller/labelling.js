var labellingService = require('service/labelling')
const uuidv4 = require('uuid/v4')
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getConsignmentLabel = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	labellingService.getConsignmentLabel(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

exports.reprintLabel = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	labellingService.reprintLabel(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.createConsignmentLabel = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	labellingService.createConsignmentLabel(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}
