var bookingService = require('service/booking')
const uuidv4 = require('uuid/v4')
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.bookConsignment = function (req, res, next) {
	console.log('bookConsignment')
	res.header('Access-Control-Allow-Origin', '*')
	bookingService.bookConsignment(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.updateConsignment = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	bookingService.updateConsignment(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.cancelConsignment = function (req, res, next) {
	console.log('cancelConsignment	')
	res.header('Access-Control-Allow-Origin', '*')
	bookingService.cancelConsignment(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}


/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.rebookConsignment = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	bookingService.rebookConsignment(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}