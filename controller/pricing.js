const pricingService = require('service/pricing')
const uuidv4 = require('uuid/v4')

exports.calculatePriceForConsignment = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	pricingService.calculatePriceForConsignment(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

exports.getEtd = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	pricingService.getEtd(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}
