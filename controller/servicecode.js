var serviceCodeService = require('service/servicecode')
const uuidv4 = require('uuid/v4')
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getAccounts = function (req, res, next) {
	serviceCodeService.getAccounts(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}
