var orderService = require('service/order')
const uuidv4 = require('uuid/v4')
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getOrderSummary = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	orderService.getOrderSummary(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}

/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.createOrder = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	orderService.createOrder(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}
