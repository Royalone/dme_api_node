var trackingService = require('service/tracking')
const uuidv4 = require('uuid/v4')
/**
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.trackAllDetails = function (req, res, next) {
	res.header('Access-Control-Allow-Origin', '*')
	trackingService.trackAllDetails(req)
		.then(response => {
			response.requestId = uuidv4().replaceAll('-', '')
			res.send(response)
		})
		.catch(error => res.status(400).send(error))
}
