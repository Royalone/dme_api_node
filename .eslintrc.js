module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true
  },
  extends: [
    'standard'
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly'
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018
  },
  plugins: [
    'react'
  ],
  rules: {
    "indent": [
        "error",
        "tab"
    ],
    "quotes": [
        "error",
        "single"
    ],
    "no-extend-native": [0, {"exceptions": ["Object"]}],
    "no-async-promise-executor": 0,
    "no-tabs": 0
  }
}
