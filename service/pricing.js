const consts = require('consts')
const logger = require('logger')
const providerConfig = require('./config')

/**
 *
 * @param {*ConsignmentBookingRequest} req
 * Booking consignment with Freight Provider
 */
exports.calculatePriceForConsignment = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const pricingService = providerConfig.PRICING_SERVICE_MAP[serviceProvider]

		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !pricingService) {
			const errorMsg = `pricingService service is disabled or invalid for service provider: ${serviceProvider}`
			logger.info(errorMsg)
			reject(new Error(errorMsg))
		}
		
		const body = req.body
		body.requestType = 'PRICING'

		pricingService.getPriceDetail(req)
			.then(response => {
				response.fpName = serviceProvider
				response.requestType = 'PRICING'
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}

exports.getEtd = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body

		const etdService = providerConfig.ETD_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !etdService) {
			logger.info(`Etd service is disabled or invalid for service provider:  ${serviceProvider}`)
			reject(new Error(`Etd service is disabled or invalid for service provider: ${serviceProvider}`))
		}
		
		const body = req.body
		body.requestType = 'GETETD'
		etdService.getEtd(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GETETD'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
