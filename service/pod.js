
var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')

exports.fetchPod = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const podService = providerConfig.POD_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !podService) {
			logger.info(`Pod service is disabled or not available for service provider: ${serviceProvider}`)
			reject(new Error(`Pod service is disabled or not available for service provider:  ${serviceProvider}`))
		}
		
		const body = req.body
		body.requestType = 'GET_POD'

		podService.fetchProofOfdelivery(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GET_POD'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}


exports.uploadPod = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const podService = providerConfig.POD_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !podService) {
			logger.info(`Pod service is disabled or not available for service provider: ${serviceProvider}`)
			reject(new Error(`Pod service is disabled or not available for service provider:  ${serviceProvider}`))
		}
		
		const body = req.body
		body.requestType = 'UPLOAD_POD'

		podService.uploadPod(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'UPLOAD_POD'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
