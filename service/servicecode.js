var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')
/**
 *
 * @param {*AccountRequest} req
 * Booking consignment with Freight Provider
 */
exports.getAccounts = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const serviceCodeService = providerConfig.SERVICECODE_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !serviceCodeService) {
			logger.info(`ServiceCode service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`ServiceCode service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'GETACCOUNTS'
		console.log('getAccounts', serviceCodeService);
		serviceCodeService.getAccounts(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GETACCOUNTS'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
