var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')

exports.trackAllDetails = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const trackingService = providerConfig.TRACKING_SERVICE_MAP[serviceProvider]

		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !trackingService) {
			logger.info(`Tracking service is disabled or invalid for service provider:  ${serviceProvider}`)
			reject(new Error(`Tracking service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'TRACKING'
		trackingService.getTrackingDetail(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'TRACKING'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
