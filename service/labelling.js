var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')
/**
 *
 * @param {*ConsignmentGetLabelRequest} req
 * get Frieight provide label with Freight Provider
 */
exports.getConsignmentLabel = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const labellingService = providerConfig.LABELLING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !labellingService) {
			logger.info(`Labelling service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Labelling service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'GET_LABEL'

		labellingService.getFreightProviderLabel(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GET_LABEL'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}

exports.reprintLabel = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const reprintLabelService = providerConfig.REPRINT_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !reprintLabelService) {
			logger.info(`Labelling service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Labelling service is disabled or invalid for service provider: ${serviceProvider}`))
		}
		
		const body = req.body
		body.requestType = 'REPRINT_LABEL'

		reprintLabelService.reprintLabel(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GET_LABEL'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}

exports.createConsignmentLabel = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const labellingService = providerConfig.LABELLING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !labellingService) {
			reject(new Error(`Labelling service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'CREATE_LABEL'

		labellingService.createFreightProviderLabel(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'CREATE_LABEL'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
