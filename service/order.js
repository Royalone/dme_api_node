var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')
exports.getOrderSummary = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const orderService = providerConfig.ORDER_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !orderService) {
			logger.info(`Get order summary service is disabled or is not available for service provider: ${serviceProvider}`)
			reject(new Error(`Get order summary service is disabled or is not available for service provider: ${serviceProvider}`))
		}
		
		const body = req.body
		body.requestType = 'GET_ORDER'

		orderService.getOrderSummary(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'GET_ORDER'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}

exports.createOrder = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const orderService = providerConfig.ORDER_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !orderService) {
			logger.info(`Create order service is disabled or is not available for service provider: ${serviceProvider}`)
			reject(new Error(`Create order service is disabled or is not available for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'CREATE_ORDER'

		orderService.createConsignmentOrder(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'CREATE_ORDER'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
