var consts = require('consts')
var logger = require('logger')
var providerConfig = require('./config')
/**
 *
 * @param {*ConsignmentBookingRequest} req
 * Booking consignment with Freight Provider
 */
exports.bookConsignment = req => {
	logger.info('bookConsignment')
	logger.info(req.body)
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const bookingService = providerConfig.BOOKING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !bookingService) {
			logger.info(`Booking service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Booking service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'BOOKING'

		bookingService.bookConsignmentWithFreightProvider(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'BOOKING'
			resolve(response)
		}).catch(error => {
			reject({errors: error})
		})
	})
}

/**
 *
 * @param {*ConsignmentBookingRequest} req
 * Booking consignment with Freight Provider
 */
exports.updateConsignment = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const bookingService = providerConfig.BOOKING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !bookingService) {
			logger.info(`Booking service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Booking service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'UPDATE_BOOK'

		bookingService.updateConsignment(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'UPDATE_BOOK'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}

/**
 *
 * @param {*ConsignmentBookingRequest} req
 * Booking consignment with Freight Provider
 */
exports.cancelConsignment = req => {	
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const bookingService = providerConfig.BOOKING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !bookingService) {
			logger.info(`Booking service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Booking service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'CANCEL_BOOK'

		bookingService.cancelConsignment(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'CANCEL_BOOK'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}

/**
 *
 * @param {*ConsignmentBookingRequest} req
 * Booking consignment with Freight Provider
 */
exports.rebookConsignment = req => {
	return new Promise(function (resolve, reject) {
		const { serviceProvider } = req.body
		const bookingService = providerConfig.BOOKING_SERVICE_MAP[serviceProvider]
		if (!consts.SERVICE_FP_ENABLE[serviceProvider] || !bookingService) {
			logger.info(`Booking service is disabled or invalid for service provider: ${serviceProvider}`)
			reject(new Error(`Booking service is disabled or invalid for service provider: ${serviceProvider}`))
		}

		const body = req.body
		body.requestType = 'REBOOK'
		
		bookingService.rebookConsignmentWithFreightProvider(req).then(response => {
			response.fpName = serviceProvider
			response.requestType = 'REBOOK'
			resolve(response)
		}).catch(error => {
			reject(error)
		})
	})
}
