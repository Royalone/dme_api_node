const consts = require('consts')

const AUSPOSTBookingService = require('providers/auspost/service/stbooking')
const AUSPOSTLabellingService = require('providers/auspost/service/stlabelling')
const AUSPOSTOrderService = require('providers/auspost/service/storder')
const AUSPOSTPricingService = require('providers/auspost/service/stpricing')
const AUSPOSTTrackingService = require('providers/auspost/service/sttracking')
const AUSPOSTPodService = require('providers/auspost/service/stpod')
const AUSPOSTServiceCodeService = require('providers/auspost/service/stservicecode')

const STBookingService = require('providers/startrack/service/stbooking')
const STLabellingService = require('providers/startrack/service/stlabelling')
const STOrderService = require('providers/startrack/service/storder')
const STPricingService = require('providers/startrack/service/stpricing')
const STTrackingService = require('providers/startrack/service/sttracking')
const STPodService = require('providers/startrack/service/stpod')
const STServiceCodeService = require('providers/startrack/service/stservicecode')
const STEtdService = require('providers/startrack/service/stetd')

const CapitalBookingService = require('providers/capital/service/capitalbooking')
const CapitalPricingService = require('providers/capital/service/capitalpricing')
const BexTrackingService = require('providers/bex/service/bextracking')

const HunterBookingService = require('providers/hunter/service/hunterbooking')
const HunterPricingService = require('providers/hunter/service/hunterpricing')
const HunterTrackingService = require('providers/hunter/service/huntertracking')
const HunterPodService = require('providers/hunter/service/hunterpod')

const HunterV2BookingService = require('providers/hunter_v2/service/hunterbooking')
const HunterV2PricingService = require('providers/hunter_v2/service/hunterpricing')
const HunterV2TrackingService = require('providers/hunter_v2/service/huntertracking')
const HunterV2LabelService = require('providers/hunter_v2/service/hunterlabel')

const AlliedBookingService = require('providers/allied/service/alliedbooking')
const AlliedPricingService = require('providers/allied/service/alliedpricing')
const AlliedTrackingService = require('providers/allied/service/alliedtracking')
const AlliedLabellingService = require('providers/allied/service/alliedlabelling')

const TNTTrackingService = require('providers/tnt/service/tnttracking')
const TNTPricingService = require('providers/tnt/service/tntpricing')
const TNTPodService = require('providers/tnt/service/tntpod')
const TNTBookingService = require('providers/tnt/service/tntbooking')
const TNTReprintService = require('providers/tnt/service/tntreprint')
const TNTLabelService = require('providers/tnt/service/tntlabel')

const FastwayPricingService = require('providers/fastway/service/fastwaypricing')
const FastwayTrackingService = require('providers/fastway/service/fastwaytracking')

const SendlePricingService = require('providers/sendle/service/sendlepricing')
const SendleBookingService = require('providers/sendle/service/sendlebooking')
const SendleTrackingService = require('providers/sendle/service/sendletracking')
const SendleLabellingService = require('providers/sendle/service/sendlelabelling')

const DhlBookingService = require('providers/dhl/service/dhlbooking')
const DhlTrackingService = require('providers/dhl/service/dhltracking')

var BOOKING_SERVICE_MAP = {}
var LABELLING_SERVICE_MAP = {}
var ORDER_SERVICE_MAP = {}
var PRICING_SERVICE_MAP = {}
var TRACKING_SERVICE_MAP = {}
var POD_SERVICE_MAP = {}
var REPRINT_SERVICE_MAP = {}
var SERVICECODE_SERVICE_MAP = {}
var ETD_SERVICE_MAP = {}

if (consts.SERVICE_FP_ENABLE.ST) {
	BOOKING_SERVICE_MAP.ST = STBookingService
	LABELLING_SERVICE_MAP.ST = STLabellingService
	ORDER_SERVICE_MAP.ST = STOrderService
	PRICING_SERVICE_MAP.ST = STPricingService
	TRACKING_SERVICE_MAP.ST = STTrackingService
	POD_SERVICE_MAP.ST = STPodService
	SERVICECODE_SERVICE_MAP.ST = STServiceCodeService
	ETD_SERVICE_MAP.ST = STEtdService
}

if (consts.SERVICE_FP_ENABLE.AUSPOST) {
	BOOKING_SERVICE_MAP.AUSPOST = AUSPOSTBookingService
	LABELLING_SERVICE_MAP.AUSPOST = AUSPOSTLabellingService
	ORDER_SERVICE_MAP.AUSPOST = AUSPOSTOrderService
	PRICING_SERVICE_MAP.AUSPOST = AUSPOSTPricingService
	TRACKING_SERVICE_MAP.AUSPOST = AUSPOSTTrackingService
	POD_SERVICE_MAP.AUSPOST = AUSPOSTPodService
	SERVICECODE_SERVICE_MAP.AUSPOST = AUSPOSTServiceCodeService
	ETD_SERVICE_MAP.AUSPOST = STEtdService
}

if (consts.SERVICE_FP_ENABLE.CAPITAL) {
	BOOKING_SERVICE_MAP.CAPITAL = CapitalBookingService
	PRICING_SERVICE_MAP.CAPITAL = CapitalPricingService
}

if (consts.SERVICE_FP_ENABLE.BEX) {
	TRACKING_SERVICE_MAP.BEX = BexTrackingService
}

if (consts.SERVICE_FP_ENABLE.TNT) {
	TRACKING_SERVICE_MAP.TNT = TNTTrackingService
	BOOKING_SERVICE_MAP.TNT = TNTBookingService
	PRICING_SERVICE_MAP.TNT = TNTPricingService
	POD_SERVICE_MAP.TNT = TNTPodService
	REPRINT_SERVICE_MAP.TNT = TNTReprintService
	LABELLING_SERVICE_MAP.TNT = TNTLabelService
}

if (consts.SERVICE_FP_ENABLE.HUNTER) {
	BOOKING_SERVICE_MAP.HUNTER = HunterBookingService
	PRICING_SERVICE_MAP.HUNTER = HunterPricingService
	TRACKING_SERVICE_MAP.HUNTER = HunterTrackingService
	POD_SERVICE_MAP.HUNTER = HunterPodService
}

if (consts.SERVICE_FP_ENABLE.HUNTER_V2) {
	BOOKING_SERVICE_MAP.HUNTER_V2 = HunterV2BookingService
	PRICING_SERVICE_MAP.HUNTER_V2 = HunterV2PricingService
	TRACKING_SERVICE_MAP.HUNTER_V2 = HunterV2TrackingService
	LABELLING_SERVICE_MAP.HUNTER_V2 = HunterV2LabelService
}

if (consts.SERVICE_FP_ENABLE.ALLIED) {
	BOOKING_SERVICE_MAP.ALLIED = AlliedBookingService
	PRICING_SERVICE_MAP.ALLIED = AlliedPricingService
	TRACKING_SERVICE_MAP.ALLIED = AlliedTrackingService
	LABELLING_SERVICE_MAP.ALLIED = AlliedLabellingService
}

if (consts.SERVICE_FP_ENABLE.FASTWAY) {
	PRICING_SERVICE_MAP.FASTWAY = FastwayPricingService
	TRACKING_SERVICE_MAP.FASTWAY = FastwayTrackingService
}

if (consts.SERVICE_FP_ENABLE.SENDLE) {
	PRICING_SERVICE_MAP.SENDLE = SendlePricingService
	BOOKING_SERVICE_MAP.SENDLE = SendleBookingService
	TRACKING_SERVICE_MAP.SENDLE = SendleTrackingService
	LABELLING_SERVICE_MAP.SENDLE = SendleLabellingService
}

if (consts.SERVICE_FP_ENABLE.DHL) {
	BOOKING_SERVICE_MAP.DHL = DhlBookingService
	TRACKING_SERVICE_MAP.DHL = DhlTrackingService
}

module.exports = {
	BOOKING_SERVICE_MAP: BOOKING_SERVICE_MAP,
	LABELLING_SERVICE_MAP: LABELLING_SERVICE_MAP,
	ORDER_SERVICE_MAP: ORDER_SERVICE_MAP,
	PRICING_SERVICE_MAP: PRICING_SERVICE_MAP,
	TRACKING_SERVICE_MAP: TRACKING_SERVICE_MAP,
	POD_SERVICE_MAP: POD_SERVICE_MAP,
	REPRINT_SERVICE_MAP: REPRINT_SERVICE_MAP,
	SERVICECODE_SERVICE_MAP: SERVICECODE_SERVICE_MAP,
	ETD_SERVICE_MAP: ETD_SERVICE_MAP
}
