
let objectMapper = require('object-mapper')
let _ = require('lodash')

let map = {
	trackingNumber: 'consignmentNumber',
	jobNumber: 'jobNumber',
	jobDate: 'jobDate',
	shippingLabel: 'shippingLabel'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.price = {
		netPrice: source.fee,
		totalTaxes: _.sumBy(source.taxes)
	}
	return destination
}
