const hunterPackageinfoMapper = require('./hunterpackageinforesponse')
const hunterConsignmentStatusMapper = require('./hunterconsignmentstatus')
const hunterTrasitStopsMapper = require('./huntertransitstops')

let objectMapper = require('object-mapper')

let map = {
	goods: [
		{
			key: 'packageDetails',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(hunterPackageinfoMapper.map(item))
				}
				return newItems
			}
		}
	],
	events: [
		{
			key: 'consignmentStatuses',
			transform: function (items) {
				let newItems = []

				if (items)
					for (let item of items) {
						newItems.push(hunterConsignmentStatusMapper.map(item))
					}
				return newItems
			}
		}
	],

	stops: [
		{
			key: 'consignmentStops',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(hunterTrasitStopsMapper.map(item))
				}
				return newItems
			}
		}
	],
	number: 'consignmentNumber',
	reference1: 'consignmentReferenceNumber'
}

exports.map = source => {
	return objectMapper(source, map)
}
