const hunterPackageinfoItemMapper = require('./hunterpackageinfoitem')
const hunterTransitExtMapper = require('./huntertransitext')
let objectMapper = require('object-mapper')

let map = {
	spAccountDetails: {
		key: 'customerCode',
		transform: function (value) {
			return value.accountCode
		}
	},
	reference1: 'reference1',
	reference2: 'reference2',
	serviceType: 'primaryService',
	connoteFormat: "connoteFormat",
	items: [
		{
			key: 'goods',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(hunterPackageinfoItemMapper.map(item))
				}
				return newItems
			}
		}
	]
}

exports.map = source => {
	let destination = objectMapper(source, map)
	let stops = []
	let readyDate = {
		earliest: source.readyDate,
		latest: source.readyDate
	}

	for (const [index, item] of source.items.entries()) {
		destination.goods[index]['description'] = item.description
	}

	source.pickupAddress.readyDate = readyDate
	source.dropAddress.readyDate = readyDate

	let puInstruction = source.pickupAddress.instruction
	source.pickupAddress.instruction = source.dropAddress.instruction
	source.dropAddress.instruction = puInstruction

	stops.push(hunterTransitExtMapper.map(source.pickupAddress))
	stops.push(hunterTransitExtMapper.map(source.dropAddress))
	destination.stops = stops

	return destination
}
