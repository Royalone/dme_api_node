let objectMapper = require('object-mapper')

let map = {
	length: {
		key: 'depth',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	width: {
		key: 'width',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	height: {
		key: 'height',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	weight: {
		key: 'weight',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	quantity: 'pieces',
	packagingType: 'typeCode'
}

exports.map = source => {
	return objectMapper(source, map)
}
