let objectMapper = require('object-mapper')
let moment = require('moment')

let map = {
	companyName: 'name',
	instruction: 'instructions',
	postalAddress: [
		{
			key: 'addressLine1',
			transform: function (value) {
				return value.address1
			}
		},
		{
			key: 'addressLine2',
			transform: function (value) {
				return value.address2
			}
		},
		{
			key: 'suburbName',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'state',
			transform: function (value) {
				return value.state
			}
		},
		{
			key: 'postCode',
			transform: function (value) {
				return value.postCode
			}
		}
	],
	readyDate: {
		key: 'timeWindow',
		transform: function (value) {
			return {
				earliest: moment(value.earliest, 'YYYY-MM-DD').format('YYYY-MM-DD') + ' 10:00',
				latest: moment(value.latest, 'YYYY-MM-DD').format('YYYY-MM-DD') + ' 20:00'
			}
		}
	}
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.contact = {
		email: source.emailAddress,
		name: source.contact,
		phone: source.phoneNumber
	}
	return destination
}
