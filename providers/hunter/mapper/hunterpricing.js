
let objectMapper = require('object-mapper')
let _ = require('lodash')
let map = {
	fee: 'netPrice',
	serviceCode: 'serviceType',
	serviceName: 'serviceName',
	etd: 'etd'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.totalTaxes = _.sumBy(source.taxes)
	return destination
}
