const hunterPackageinfoItemMapper = require('./hunterpackageinfoitem')
const hunterPriceTransitExtMapper = require('./hunterpricetransitext')
let objectMapper = require('object-mapper')

let map = {
	items: [
		{
			key: 'goods',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(hunterPackageinfoItemMapper.map(item))
				}
				return newItems
			}
		}
	],
	spAccountDetails: {
		key: 'customerCode',
		transform: function (value) {
			return value.accountCode
		}
	}
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.fromLocation = hunterPriceTransitExtMapper.map(source.pickupAddress)
	destination.toLocation = hunterPriceTransitExtMapper.map(source.dropAddress)
	return destination
}
