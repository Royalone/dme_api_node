const logger = require('logger')
const hunterHttpClient = require('../httpclient')
const hunterBaseRequestValidator = require('../validator/hunterbaserequest')
const hunterTrackingResponseMapper = require('../mapper/huntertrackingresponse')

/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails, spAccountDetails } = req.body
		let response = {}
		response.consignmentTrackDetails = []

		if (consignmentDetails.length === 0) {
			resolve(response)
			return
		}

		for (const consignmentDetail of consignmentDetails) {
			hunterHttpClient.getJobStatuses(consignmentDetail.consignmentNumber, spAccountDetails.accountCode, spAccountDetails.accountKey)
				.then(hunterTrackResponse => {
					let trackingDetails = hunterTrackingResponseMapper.map(hunterTrackResponse)

					// Convert "'" into "`"
					trackingDetails = JSON.stringify(trackingDetails)
					trackingDetails = trackingDetails.replace("'", "`");

					response.consignmentTrackDetails.push(JSON.parse(trackingDetails))
					resolve(response)
				}).catch(error => {
					reject(error)
				})
		}
	})
}
