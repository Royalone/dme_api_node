const hunterPricingRequestMapper = require('../mapper/hunterpricingrequest')
const hunterHttpClient = require('../httpclient')
const hunterBaseRequestValidator = require('../validator/hunterbaserequest')
const hunterPricingResponseMapper = require('../mapper/hunterpricingresponse')
/**
 *
 * @param {*} req
 */
exports.getPriceDetail = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const hunterPricingRequest = hunterPricingRequestMapper.map(req.body)
		const { accountKey } = req.body.spAccountDetails
		hunterHttpClient.pricingConsignment(hunterPricingRequest, accountKey)
			.then(response => {
				resolve(hunterPricingResponseMapper.map(response))
			}).catch(error => {
				reject(error)
			})
	})
}
