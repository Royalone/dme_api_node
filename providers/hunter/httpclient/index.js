const unirest = require('unirest')
const logger = require('logger')

exports.createAuthHeaders = (accountKey) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: this.createAuthorizationApiKey(accountKey)
	}
}

exports.createAuthorizationApiKey = (accountKey) => {
	return 'Basic ' + accountKey
}

exports.bookConsignment = (bookingRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_BOOKING,
		method: 'post',
		data: bookingRequest
	}

	logger.info('HUNTER BOOK Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		unirest.post(process.env.SERVICE_FP_HUNTER_BOOKING)
			.headers(headers)
			.strictSSL(false)
			.send(bookingRequest)
			.then(response => {
				if (response.code >= 300) {
					logger.info('HUNTER BOOK Failed: ' +  JSON.stringify(response.body, null, 4))
					reject(response.body)
				} else {
					let responseBody = Object.assign({}, response.body)
					responseBody.shippingLabel = responseBody.shippingLabel.slice(0, 10) + "..."
					logger.info('HUNTER BOOK Success: ' +  JSON.stringify(responseBody, null, 4))
					resolve(response.body)
				}
			}).catch(error => {
				logger.info('HUNTER BOOK Failed: ' +  JSON.stringify(error, null, 4))
				reject(error)
			})
	})
}

exports.pricingConsignment = (pricingRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_PRICING,
		method: 'post',
		data: pricingRequest
	}

	logger.info('HUNTER Pricing Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		unirest.post(process.env.SERVICE_FP_HUNTER_PRICING)
			.headers(headers)
			.strictSSL(false)
			.send(pricingRequest)
			.then(response => {
				if (response.code >= 300) { 
					logger.info('HUNTER Pricing Failed: ' +  JSON.stringify(response.body, null, 4))
					reject(response.body)
				} else { 
					logger.info('HUNTER Pricing Success: ' +  JSON.stringify(response.body, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				logger.info('HUNTER Pricing Failed: ' +  JSON.stringify(error, null, 4))
				reject(error)
			})
	})
}

exports.getJobStatuses = (trackingNumber, customerCode, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	return new Promise(function (resolve, reject) {
		let request = {
			customerCode: customerCode,
			trackingNumber: trackingNumber
		}

		logger.info('HUNTER Tracking Payload: ' + JSON.stringify(request))

		unirest.get(process.env.SERVICE_FP_HUNTER_TRACKING)
			.headers(headers)
			.strictSSL(false)
			.query(request)
			.then(response => {
				if (response.code >= 300) { 
					logger.info('HUNTER Tracking Failed: ' +  JSON.stringify(response.body, null, 4))
					reject(response.body)
				} else { 
					logger.info('HUNTER Tracking Response: ' +  JSON.stringify(response.body, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				logger.info('HUNTER Tracking Failed: ' +  JSON.stringify(error, null, 4))
				reject(error)
			})
	})
}

exports.fetchPod = (jobNumber, jobDate, customerCode, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	return new Promise(function (resolve, reject) {
		let request = {
			customerCode: customerCode,
			jobNumber: jobNumber,
			jobDate: jobDate
		}

		logger.info('HUNTER POD Payload: ' + JSON.stringify(request))

		unirest.get(process.env.SERVICE_FP_HUNTER_POD)
			.headers(headers)
			.strictSSL(false)
			.query(request)
			.then(response => {
				if (response.code >= 300) {
					logger.info('HUNTER POD Failed: ' +  JSON.stringify(response.body, null, 4))
					reject(response.body)
				} else { 
					let responseBody = JSON.parse(JSON.stringify(response.body))
					responseBody[0]["podImage"] = responseBody[0]["podImage"].slice(0, 10) + "..."

					logger.info('HUNTER POD Success: ' +  JSON.stringify(responseBody, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				// let errorMessage = error.errorMessage
				// errorMessage = errorMessage.replace('\'', '')
				// error.errorMessage = errorMessage
				// reject(error)
				logger.info('HUNTER POD Failed: ' +  JSON.stringify(error, null, 4))
				reject(error)
			})
	})
}
