exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountKey || spAccountDetails.accountKey === '') {
			validated = false
			throw new Error('Account key cannot be empty for calling hunter services')
		}

		if (!spAccountDetails.accountCode || spAccountDetails.accountCode === '') {
			validated = false
			throw new Error('Account code cannot be null or empty for calling hunter services')
		}
	} catch (error) {
		reject({ errorMessage: error.message })
	}

	return validated
}
