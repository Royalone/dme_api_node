const dhlHttpClient = require('../httpclient')
const dhlBaseRequestValidator = require('../validator/dhlbaserequest')
const dhlTrackingResponseMapper = require('../mapper/dhltrackingresponse')
/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise(function (resolve, reject) {
		if (!dhlBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails, spAccountDetails } = req.body

		let consignmentNos = []
		let countryCodes = []

		for (const cngDetails of consignmentDetails) {
			consignmentNos.push(cngDetails.consignmentNumber)
			countryCodes.push(cngDetails.destinationPostcode)
		}

		dhlHttpClient.trackShipping(consignmentNos.join(), spAccountDetails.accountKey, spAccountDetails.accountPassword, countryCodes.join())
			.then(response => {
				if (response) { resolve(dhlTrackingResponseMapper.map(response)) } else { resolve(response) }
			}).catch(error => {
				reject(error)
			})
	})
}
