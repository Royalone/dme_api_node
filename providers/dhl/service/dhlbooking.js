const dhlBookingRequestMapper = require('../mapper/dhlbookingrequest')
const dhlHttpClient = require('../httpclient')
const dhlBaseRequestValidator = require('../validator/dhlbaserequest')

/**
 *
 * @param {*} req
 */
exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(function (resolve, reject) {
		if (!dhlBaseRequestValidator.validateRequest(req, reject)) {
			return
		}
		const { spAccountDetails } = req.body
		const request = dhlBookingRequestMapper.map(req.body)
		dhlHttpClient.bookShipments(request, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve({
					consignmentNumber: response.purchase_order_number,
					orderNumber: response.order_number,
					labelType: response.label_type,
					headPort: response.head_port,
					carrierService: response.carrier_service,
					customerId: response.customer_id,
					facilityId: response.facility_id
				})
			}).catch(error => {
				reject(error)
			})
	})
}

exports.updateConsignment = (req) => {
	return new Promise(function (resolve, reject) {
		if (!dhlBaseRequestValidator.validateRequest(req, reject)) {
			return
		}
		const { spAccountDetails, consignmentNumber } = req.body
		const request = dhlBookingRequestMapper.map(req.body)
		dhlHttpClient.updateShipment(request, consignmentNumber, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				if (response.consignment_number) { resolve({ consignmentNumber: response.consignment_number }) } else { resolve({ consignmentNumber: consignmentNumber }) }
			}).catch(error => {
				reject(error)
			})
	})
}
