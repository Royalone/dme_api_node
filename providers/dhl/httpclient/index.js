const axios = require('axios')
const Base64 = require('base64util')
const logger = require('logger')

exports.createAuthHeaders = (accountKey, accountPasswd) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: this.createAuthorizationApiKey(accountKey, accountPasswd)
	}
}

const orderAccountKey = "DELIVER_ME_API"
const orderAccountPasswd = "ZGVsaXZlcm1lYXBpdXNlcjEyMw=="

exports.createAuthorizationApiKey = (accountKey, accountPasswd) => {
	return 'Basic ' + Base64.encode(accountKey + ':' + accountPasswd)
}

exports.bookShipments = (bookingRequest, accountKey, accountPasswd) =>  {
	let headers = this.createAuthHeaders(accountKey, accountPasswd)
	let params = {
		headers: headers,
		url: process.env.SERVICE_FP_DHL_BOOKING,
		data: bookingRequest,
		method: 'post'
	}

	let response

	return new Promise(async (resolve, reject) => {
		try {
			if(bookingRequest.clientType != "aldi") {
				logger.info('DHL Carrier Params: ' + JSON.stringify(params, null, 4))

				response = await axios(params)

				if(response.status < 300) {
					bookingRequest.purchase_order_number = response.data.consignment_number
				}
				else {
					reject(response.response.data.errors)
				}
			}

			delete bookingRequest.clientType
			bookingRequest.receiver_address.address_id = bookingRequest.purchase_order_number

			headers = this.createAuthHeaders(orderAccountKey, orderAccountPasswd)

			params = {
				headers: headers,
				url: process.env.SERVICE_FP_DHL_BOOKING,
				data: bookingRequest,
				method: 'post'
			}

			logger.info('DHL Booking Params: ' + JSON.stringify(params, null, 4))

			response = await axios(params)
			resolve(response.data)

			if(response.status < 300) {
				logger.info('DHL Booking Response: ' + JSON.stringify(response.data, null, 4))
				resolve(response.data)
			}
			else {
				reject(response.response.data.errors)
			}
		} catch(error) {
			reject(error)
		}
	})
}

exports.updateShipment = (shipmentRequest, shipmentId, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountKey, accountPasswd)
	
	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_DHL_BOOKING + '/' + shipmentId,
		data: shipmentRequest,
		method: 'put'
	}

	logger.info('DHL UpdateBook Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('DHL UpdateBook Response: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			reject(error.response.data.errors)
		})
	})
}

exports.trackShipping = (trackingNumber, accountKey, accountPasswd, countryCode) => {
	accountKey = "DELIVER_ME_CARRIER_API"
	accountPasswd = "RGVsaXZlcmNhcnJpZXJhcGkxMjM="
	const headers = this.createAuthHeaders(accountKey, accountPasswd)

	const params = {
		headers: headers,
		params: {
			num: trackingNumber,
			c: countryCode,
			p: 90
		},
		method: 'get'
	}

	logger.info('DHL Tracking Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(process.env.SERVICE_FP_DHL_TRACKING, params).then(response => {
			logger.info('DHL Tracking Response: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			reject(error.response.data.errors)
		})
	})
}
