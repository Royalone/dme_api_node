let objectMapper = require('object-mapper')

let map = {
	length: {
		key: 'length',
		transform: function (value) {
			return String(value)
		}
	},
	height: {
		key: 'height',
		transform: function (value) {
			return String(value)
		}
	},
	width: {
		key: 'width',
		transform: function (value) {
			return String(value)
		}
	},
	quantity: {
		key: 'quantity',
		transform: function (value) {
			return String(value)
		}
	},
	dangerous: 'dangerous_goods',
	volume: {
		key: 'volume',
		transform: function (value) {
			return String(value)
		}
	},
	description: 'goods_description',
	weight: {
		key: 'gross_weight',
		transform: function (value) {
			return String(value)
		}
	},
	packagingType: 'unit_of_measure',
	packageCode: 'package_code'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.unit_of_measure = "PAL"
	destination.net_weight = "6"	
	return destination
}
