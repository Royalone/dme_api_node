let objectMapper = require('object-mapper')

let map = {
	emailAddress: 'email',
	phoneNumber: 'phone_number',
	contact: 'name',
	postalAddress: [
		{
			key: 'address_1',
			transform: function (value) {
				return value.address1
			}
		},
		{
			key: 'city',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'post_code',
			transform: function (value) {
				return value.postCode
			}
		},
		{
			key: 'state',
			transform: function (value) {
				return value.state
			}
		}
	]
}

exports.map = source => {
	return objectMapper(source, map)
}
