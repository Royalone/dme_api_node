let objectMapper = require('object-mapper')

let map = {
	stop_event: 'stopType',
	carrier_code: 'carrier_code',
	carrier_name: 'carrier_name',
	actual_arrival: 'actual_arrival',
	actual_departure: 'actual_departure',
	planned_arrival: 'planned_arrival',
	planned_departure: 'planned_departure',
	carrier_connote_ref: 'carrier_connote_ref'

}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.postalAddress = {
		address1: source.address
	}
	return destination
}
