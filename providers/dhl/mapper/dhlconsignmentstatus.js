
let objectMapper = require('object-mapper')

let map = {
	type: 'type',
	orig_code: 'status',
	name: 'depotName',
	issued: 'statusDate',
	comment: 'comment',
	address: 'address',
	description: 'description',
	code: 'statusCode'
}

exports.map = source => {
	return objectMapper(source, map)
}
