let objectMapper = require('object-mapper')

let map = {
	gross_weight: 'weight',
	volume: 'volume',
	dangerous_goods: {
		key: 'dangerous',
		transform: function (value) {
			if (value) { return true } else { return false }
		}
	}
}

exports.map = source => {
	return objectMapper(source, map)
}
