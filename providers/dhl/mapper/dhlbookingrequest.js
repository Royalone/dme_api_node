const dhlPackageinfoItemMapper = require('./dhlpackageinfoitem')
const dhlTransitExtMapper = require('./dhltransitext')
let objectMapper = require('object-mapper')

let map = {
	items: [
		{
			key: 'line_items',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(dhlPackageinfoItemMapper.map(item))
				}
				return newItems
			}
		}
	],
	pickupAddress: {
		key: 'loading_address',
		transform: function (value) {
			return dhlTransitExtMapper.map(value)
		}
	},
	dropAddress: {
		key: 'receiver_address',
		transform: function (value) {
			return dhlTransitExtMapper.map(value)
		}
	},
	clientType: 'clientType',
	consignmentNoteNumber: 'purchase_order_number',
	orderNumber: 'order_number'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.customer_id = "CID229023"
	destination.facility_id = "AU_0207"
	destination.carrier_service = "PFM|PFM"
	return destination
}
