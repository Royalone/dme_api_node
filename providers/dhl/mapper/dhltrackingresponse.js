const dhlPackageinfoMapper = require('./dhlpackageinforesponse')
const dhlConsignmentStatusMapper = require('./dhlconsignmentstatus')
const dhlTrasitStopsMapper = require('./dhltransitstops')

let objectMapper = require('object-mapper')

let map = {
	units: [
		{
			key: 'packageDetails',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(dhlPackageinfoMapper.map(item))
				}
				return newItems
			}
		}
	],
	transport_history: [
		{
			key: 'consignmentStatuses',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(dhlConsignmentStatusMapper.map(item))
				}
				return newItems
			}
		}
	],
	total_trasport_units: 'totalItems',
	total_volume: 'totalVolume',
	total_weight: 'totalWeight',
	total_weight_unit: 'weightUnit',
	total_volume_unit: 'volumeUnit',
	units_count: 'totalItems',
	estimated_delivery_date: 'scheduledDeliveryDate',
	actual_delivery_date: 'deliveryDate',
	actual_dispatch_date: 'pickupDate'
}

exports.createTransitStops = source => {
	let transitStops = []
	transitStops.push(this.createSourceTransitStop(source))

	if (source.transport_stops) {
		for (const transportStop of source.transport_stops) {
			transitStops.push(dhlTrasitStopsMapper.map(transportStop))
		}
	}

	transitStops.push()
	transitStops.push(this.createDestinationTransitStop(source))
}

exports.createSourceTransitStop = source => {
	let sourcePostalAddress = {}
	let sourceTransitStop = {}

	sourcePostalAddress.country = source.consignor_country
	sourcePostalAddress.postCode = source.consignor_zip
	sourcePostalAddress.suburb = source.consignor_region
	sourcePostalAddress.city = source.consignor_city

	sourceTransitStop.stopType = 'SRC'
	sourceTransitStop.postalAddress = sourcePostalAddress
}

exports.createDestinationTransitStop = source => {
	let dstPostalAddress = {}
	let dstTransitStop = {}

	dstPostalAddress.country = source.consignee_country
	dstPostalAddress.postCode = source.consignee_zip
	dstPostalAddress.suburb = source.consignee_region
	dstPostalAddress.city = source.consignee_city

	dstTransitStop.stopType = 'DST'
	dstTransitStop.postalAddress = dstPostalAddress
}

exports.map = source => {
	let consignmentTrackDetails = objectMapper(source, map)
	consignmentTrackDetails.consignmentStatuses.push({
		type: source.type,
		status: source.status,
		statusDate: source.status_date
	})

	consignmentTrackDetails.consignmentStops = this.createTransitStops(source)

	let consignmentTrackResponse = { consignmentTrackDetails: [] }

	consignmentTrackResponse.consignmentTrackDetails.push(consignmentTrackDetails)
	return consignmentTrackResponse
}
