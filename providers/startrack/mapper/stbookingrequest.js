const objectMapper = require('object-mapper')

const stpackageinfoitemMapper = require('./stpackageinfoitem')
const staddressMapper = require('./staddress')

const map = {
	pickupAddress: [
		{
			key: 'from',
			transform: function (value) {
				return staddressMapper.map(value)
			}
		}
	],
	dropAddress: [
		{
			key: 'to',
			transform: function (value) {
				return staddressMapper.map(value)
			}
		}
	],
	items: [
		{
			key: 'items',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(stpackageinfoitemMapper.map(item))
				}
				return newItems
			}
		},
		{
			key: 'email_tracking_enabled',
			transform: function (value) {
				return true
			}
		}
	],
	consignmentNumber: 'consignmentNumber',
	referenceNumber: 'referenceNumber',
	serviceType: 'serviceType',
	bookedBy: 'bookedBy',
	readyDate: 'readyDate'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.email_tracking_enabled = true
	destination.consolidate = false
	return destination
}
