let objectMapper = require('object-mapper')
const uuidv4 = require('uuid/v4')

let map = {
	length: 'length',
	width: 'width',
	height: 'height',
	weight: 'weight',
	itemId: 'product_id',
	packagingType: 'packaging_type',
	other: [
		{
			key: 'authority_to_leave',
			transform: function (value) {
				return false
			}
		},
		{
			key: 'allow_partial_delivery',
			transform: function (value) {
				return false
			}
		},
		{
			key: 'item_reference',
			transform: function (value) {
				return uuidv4()
			}
		}
	]
}

exports.map = source => {
	return objectMapper(source, map)
}
