
const stShipmentSummaryToPriceMapper = require('./stshipmentsummarytoprice')
let objectMapper = require('object-mapper')

let map = {
	shipment_summary: {
		key: 'price',
		transform: function (value) {
			return stShipmentSummaryToPriceMapper.map(value)
		}
	},
	shipment_id: 'consignmentNumber',
	shipment_reference: 'consignmentReferenceNumber',
	movement_type: 'status',
	items: 'items'
}

exports.map = source => {
	return objectMapper(source, map)
}
