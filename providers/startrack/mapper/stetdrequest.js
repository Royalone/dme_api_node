const objectMapper = require('object-mapper')

const stpackageinfoitemMapper = require('./stpackageinfoitem')
const staddressMapper = require('./staddress')

const map = {
	pickupAddress: [
		{
			key: 'from',
			transform: function (value) {
				return staddressMapper.map(value)
			}
		}
	],
	dropAddress: [
		{
			key: 'to',
			transform: function (value) {
				return staddressMapper.map(value)
			}
		}
	],
	product_ids: 'product_ids',
	readyDate: 'despatch_date'
}

Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(),
          (mm>9 ? '' : '0') + mm,
          (dd>9 ? '' : '0') + dd
         ].join('-');
};

exports.map = source => {
	let destination = objectMapper(source, map)

	destination.despatch_date = new Date(source.readyDate).yyyymmdd()
	return destination
}
