const objectMapper = require('object-mapper')

const map = {
	despatch_date: 'despatch_date',
	'estimated_delivery_dates': 'estimated_delivery_dates',
	'from': 'from',
	'to': 'to'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	return destination
}
