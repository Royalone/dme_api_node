
const stPackageInfoValidator = require('./stpackageinfo')

exports.validateRequest = (req, reject) => {
	const { items } = req.body
	let validated = true

	try {
		for (const item of items) {
			if (!stPackageInfoValidator.validateRequest(item)) { validated = false }
		}
	} catch (error) {
		reject({ errorMessage: error })
	}
	return validated
}
