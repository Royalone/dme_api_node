
const stPackageTypeValidator = require('./stpackagetype')

exports.validateRequest = (packageInfo) => {
	let validated = true

	if (packageInfo === null) {
		validated = false
		throw new Error('ST item cannot be null. Please configure valid item details in the request')
	} else if (!packageInfo.itemId || packageInfo.itemId === null || packageInfo.itemId === '') {
		validated = false
		throw new Error('ST item id is mandatory. Please configure valid item id for item in the request')
	} else {
		validated = stPackageTypeValidator.validateRequest(packageInfo.packagingType)
	}
	return validated
}
