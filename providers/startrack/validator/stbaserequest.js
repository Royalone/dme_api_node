exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountCode || spAccountDetails.accountCode === '') {
			validated = false
			throw new Error('Service provider account code cannot be null or empty in the request. Must contain valid star track account number')
		}

		if (!spAccountDetails.accountPassword || spAccountDetails.accountPassword === '') {
			validated = false
			throw new Error('Service provider account password cannot be null or empty for star track service requests')
		}

		if (!spAccountDetails.accountKey || spAccountDetails.accountKey === '') {
			validated = false
			throw new Error('Service provider account key cannot be null or empty for star track service requests')
		}
	} catch (error) {
		reject({ errorMessage: error.message })
	}

	return validated
}
