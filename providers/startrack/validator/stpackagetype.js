
const stPackageTypes = [
	'CTN',
	'PAL',
	'SAT',
	'BAG',
	'ENV',
	'ITM',
	'JIF',
	'SKI'
]

exports.validateRequest = (packageType) => {
	let validated = true
	if (packageType === null) {
		validated = false
		throw new Error(`Package type field is mandatory for ST service providers. Valid values are ${stPackageTypes.toString()}`)
	} else {
		if (stPackageTypes.indexOf(packageType) < 0) {
			validated = false
			throw new Error(`Invalid package type ${packageType} in package for ST service providers. Valid values are ${stPackageTypes.toString()}`)
		}
	}
	return validated
}
