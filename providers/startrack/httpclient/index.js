const Base64 = require('base64util')
const axios = require('axios')
const logger = require('logger')
exports.createAuthHeaders = (accountNumber, accountKey, accountPasswd) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		'Account-Number': accountNumber,
		Authorization: this.createAuthorizationApiKey(accountKey, accountPasswd)
	}
}

exports.createAuthorizationApiKey = (accountKey, accountPasswd) => {
	return 'Basic ' + Base64.encode(accountKey + ':' + accountPasswd)
}

exports.bookShipments = (bookingRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_BOOKING,
		data: {
			shipments: bookingRequest
		},
		method: 'post'
	}

	logger.info('STARTRACK Booking Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		// let fakeError = {
		// 	"errors": [
		// 		{
		// 			"code": '400',
		//        		"name": 'Bad Request',
		//        		"message": 'Name longer than 40 characters: Melbourne Cleaning Supplies (Rapid Clean Group)',
		//        		"field": 'shipments[0].to.destination.addressee.name'
		//        	}]
		//     };
		// reject(fakeError)

		axios(params).then(response => {
			logger.info('STARTRACK Booking Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK Booking Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data)
		})
	})
}



exports.getEtd = (etdRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_ETD,
		data: etdRequest,
		method: 'post'
	}

	logger.info('STARTRACK Etd Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK Etd Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK Etd Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data)
		})
	})
}


exports.updateShipment = (shipmentRequest, shipmentId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_BOOKING + '/' + shipmentId,
		data: shipmentRequest,
		method: 'put'
	}

	logger.info('STARTRACK EditBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK EditBook Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK EditBook Failed: ' + JSON.stringify(error.response.data.errors, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.cancelShipments = (consignmentIds, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	let stShipmentIds = consignmentIds.join()

	const params = {
		headers: headers,
		params: {
			shipment_ids: stShipmentIds
		},
		method: 'delete'
	}

	logger.info('STARTRACK CancelBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.delete(process.env.SERVICE_FP_ST_BOOKING, params)
			.then(response => {
				logger.info('STARTRACK CancelBook Success')
				resolve(response.data)
			}).catch(error => {
				console.log('STARTRACK CancelBook Failed', error)
				logger.info('STARTRACK CancelBook Failed')
				reject(error.response.data.errors)
			})
	})
}

exports.calculateShipmentPrice = (stPricingRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_PRICING,
		data: {
			shipments: stPricingRequest
		},
		method: 'post'
	}
	logger.info('STARTRACK Pricing Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK Pricing Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK Pricing Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.createLabel = (createLabelRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_LABELLING_CREATE,
		data: createLabelRequest,
		method: 'post'
	}
	logger.info('STARTRACK CreateLabel Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK CreateLabel Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK CreateLabel Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getLabel = (requestId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_LABELLING_GET + '/' + requestId,
		method: 'get'
	}
	logger.info('STARTRACK GetLabel Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK GetLabel Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK GetLabel Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getOrderSummary = (requestId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)
	delete headers.Accept

	const stOrderSummaryUrl = process.env.SERVICE_FP_ST_ORDER_SUMMARY + '/' + accountNumber + '/orders/' + requestId + '/summary'

	const params = {
		responseType: 'arraybuffer',
		headers: headers,
		method: 'get'
	}

	logger.info('STARTRACK GetOrderSummary Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(stOrderSummaryUrl, params).then(response => {

			logger.info('STARTRACK GetOrderSummary Success: ' + response.data)
			resolve({
				orderSummaryPdf: response.data,
				orderId: requestId
			})
		}).catch(error => {
			logger.info('STARTRACK GetOrderSummary Failed: ' + error.response.data)
			reject(error.response.data.errors)
		})
	})
}

exports.createOrder = (orderRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_ORDER_CREATE,
		data: orderRequest,
		method: 'put'
	}

	logger.info('STARTRACK CreateOrder Payload: ' + JSON.stringify(params, null, 4))

	return new Promise((resolve, reject) => {
		axios(params).then(response => {
			logger.info('STARTRACK CreateOrder Success: ')
			resolve(response)
		}).catch(error => {
			logger.info('STARTRACK CreateOrder Failed: ' + error)
			reject(error.response)
		})
	})
}

exports.trackShipping = (trackingNumber, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		params: {
			tracking_ids: trackingNumber
		},
		method: 'get'
	}

	logger.info('STARTRACK Tracking Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(process.env.SERVICE_FP_ST_TRACKING, params).then(response => {
			logger.info('STARTRACK Tracking Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK Tracking Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}


exports.getAccounts = (accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_ST_GET_ACCOUNTS + '/' + accountNumber,
		method: 'get'
	}
	logger.info('STARTRACK GetAccounts Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('STARTRACK GetAccounts Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('STARTRACK GetAccounts Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}
