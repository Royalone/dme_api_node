const stEtdRequestMapper = require('../mapper/stetdrequest')
const stEtdResponseMapper = require('../mapper/stetdresponse')
const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')
const stBookingValidator = require('../validator/stbooking')

/**
 *
 * @param {*} req
 */
exports.getEtd = (req) => {
	console.log('GETETD request')
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const stShipmentRequest = stEtdRequestMapper.map(req.body)
		let stShipments = []
		const { spAccountDetails } = req.body
		stHttpClient.getEtd(stShipmentRequest, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(stEtdResponseMapper.map(response))
			}).catch(ex => {
				let errors = {}
				reject(errors)
			})
	})
}
