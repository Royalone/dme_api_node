/**
 *
 * @param {*} req
 */

const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')
const objectMapper = require('object-mapper')

exports.getFreightProviderLabel = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentNumber, spAccountDetails } = req.body

		stHttpClient.getLabel(consignmentNumber, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}

/**
 *
 * @param {*} req
 */
exports.createFreightProviderLabel = (req) => {
	const self = this
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { spAccountDetails } = req.body
		const stCreateLabelRequest = self.convertToSTCreateLabelRequest(req)

		stHttpClient.createLabel(stCreateLabelRequest, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(response.labels)
			}).catch(error => {
				reject(error)
			})
	})
}

exports.convertToSTCreateLabelRequest = (req) => {
	let preferenceList = []
	let map = {
		format: 'format',
		type: 'type'
	}
	const preferences = objectMapper(req.body, map)

	let groupList = []
	for (let pageFormat of req.body.pageFormat) {
		let pageFormatMap = {
			typeOfPost: 'group',
			branded: 'branded',
			layout: 'layout',
			leftOffset: 'left_offset',
			topOffset: 'top_offset'
		}
		const group = objectMapper(pageFormat, pageFormatMap)
		groupList.push(group)
	}
	preferences.groups = groupList
	preferenceList.push(preferences)

	let shipmentList = []
	let itemList = []
	let shipment = { shipment_id: req.body.consignmentNumber }

	for (let item of req.body.items) {
		itemList.push({ item_id: item.itemId, authority_to_leave: false, allow_partial_delivery: false })
	}

	shipment.items = itemList
	shipmentList.push(shipment)

	return {
		preferences: preferenceList,
		shipments: shipmentList
	}
}
