const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')
const objectMapper = require('object-mapper')
const fs = require('fs')

exports.savePDF = (arrayBuffer) => {
	const uint8Array = new Uint8Array(arrayBuffer)

	let buf = Buffer.from(uint8Array) // decode

	fs.writeFile('test.pdf', buf, function (err) {
	})
}

/**
 * return Ordersummary PDF raw data with OrderID
 * @param {*ConsignmentOrderSummaryRequest} req
 */
exports.getOrderSummary = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { orderId, spAccountDetails } = req.body
		stHttpClient.getOrderSummary(
			orderId,
			spAccountDetails.accountCode,
			spAccountDetails.accountKey,
			spAccountDetails.accountPassword
		)
			.then(response => {
				resolve({
					pdfData: response.orderSummaryPdf,
					orderId: response.orderId
				})
			}).catch(error => {
				reject(error)
			})
	})
}

/**
 *
 * @param {*ConsignmentCreateOrderRequest} req
 */
exports.createConsignmentOrder = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { spAccountDetails } = req.body
		let map = {
			consignmentNumbers: [
				{
					key: 'shipments',
					transform: function (value) {
						let shipmentIds = []
						for (let number of value) {
							shipmentIds.push({ shipment_id: number })
						}
						return shipmentIds
					}
				}
			]
		}

		const orderRequest = objectMapper(req.body, map)
		orderRequest.order_reference = 'My order reference'
		orderRequest.payment_method = 'CHARGE_TO_ACCOUNT'
		stHttpClient.createOrder(
			orderRequest,
			spAccountDetails.accountCode,
			spAccountDetails.accountKey,
			spAccountDetails.accountPassword
		)
			.then(response => {
				resolve(response.data.order)
			})
			.catch(error => {
				reject(error)
			})
	})
}
