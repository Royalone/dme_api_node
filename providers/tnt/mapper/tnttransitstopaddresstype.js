let objectMapper = require('object-mapper')

let map = {
	postalAddress: [
		{
			key: 'suburb',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'postCode',
			transform: function (value) {
				return value.postCode
			}
		},
		{
			key: 'state',
			transform: function (value) {
				return value.state
			}
		}
	]
}

exports.map = source => {
	return objectMapper(source, map)
}
