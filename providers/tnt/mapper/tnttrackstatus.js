let objectMapper = require('object-mapper')

let map = {
	depotName: 'depotName',
	description: 'description',
	signatory: 'signatory',
	statusCode: 'status',
	statusDescription: 'statusDescription',
	date: 'statusDate'
}

exports.map = source => {
	return objectMapper(source, map)
}
