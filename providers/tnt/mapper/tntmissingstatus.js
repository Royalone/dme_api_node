let objectMapper = require('object-mapper')

let map = {
	conId: 'consignmentNumber'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	let statusList = []
	statusList.push({
		status: 'NOT_FOUND'
	})

	destination.consignmentStatuses = statusList
	return destination
}
