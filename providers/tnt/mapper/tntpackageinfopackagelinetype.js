const tntPackageWeightTypeMapper = require('./tntpackageweighttype')
const tntPackageDimensionTypeMapper = require('./tntpackagedimensiontype')

exports.map = source => {
	let destination = {}
	destination.numberOfPackages = parseInt(source.quantity)
	destination.dimensions = tntPackageDimensionTypeMapper.map(source)
	destination.weight = tntPackageWeightTypeMapper.map(source)
	return destination
}
