let objectMapper = require('object-mapper')

let map = {
	companyName: 'Name',
	postalAddress: [
		{
			key: 'Address1',
			transform: function (value) {
				return value.address1
			}
		},
		{
			key: 'Address2',
			transform: function (value) {
				return value.address2
			}
		},
		{
			key: 'Suburb',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'State',
			transform: function (value) {
				return value.state
			}
		},
		{
			key: 'Postcode',
			transform: function (value) {
				return value.postCode
			}
		}
	],
	contact: 'ContactName',
	phoneNumber: 'Phone'
}

exports.map = source => {
	return objectMapper(source, map)
}
