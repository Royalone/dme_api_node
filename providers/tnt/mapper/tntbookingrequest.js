let objectMapper = require('object-mapper')
const tntTransitStopAddress = require('./tnttransitstopaddress')

let map = {
	spAccountDetails: {
		key: 'SenderAccount',
		transform: function (value) {
			return value.accountCode
		}
	},

	pickupAddress: [
		{
			key: 'SenderDetails',
			transform: function (value) {
				return tntTransitStopAddress.map(value)
			}
		}
	],

	pickupAddressCopy: [
		{
			key: 'CollectionDetails',
			transform: function (value) {
				return tntTransitStopAddress.map(value)
			}
		}
	],

	dropAddress: [
		{
			key: 'ReceiverDetails',
			transform: function (value) {
				return tntTransitStopAddress.map(value)
			}
		}
	],

	itemCount: 'ItemCount',
	totalWeight: 'TotalWeight',
	maxLength: 'MaxLength',
	maxWidth: 'MaxWidth',
	maxHeight: 'MaxHeight',
	packagingCode: 'PackagingCode',
	collectionDateTime: 'CollectionDateTime',
	collectionCloseTime: 'CollectionCloseTime',
	serviceCode: 'ServiceCode',
	isDangerousGoods: 'IsDangerousGoods',
	payer: 'Payer',
	receiver_Account: 'ReceiverAccount',
	consignmentNoteNumber: 'ConsignmentNoteNumber',
	customerReference: 'CustomerReference',
	collectionInstructions: 'Specialinstructions'
}

exports.map = source => {
	return objectMapper(source, map)
}
