const tntLabelAddressMapper = require('./tntlabeladdress')

exports.map = source => {
	const ConsignmentProcessList = {}

	const PackageLines = []

	for (const item of source.items) {
		let PackageLine = {}
		PackageLine.lineSequence = PackageLines.length + 1
		PackageLine.lineCustomerReference = item.gapRa
		PackageLine.descriptionOfGoods = item.description
		PackageLine.itemQuantity = item.quantity
		PackageLine.lineWeight = item.weight
		PackageLine.itemLength = item.length
		PackageLine.itemWidth = item.width
		PackageLine.itemHeight = item.height
		PackageLine.dimensionMeasureUnit = 'CM'
		PackageLine.lineVolume = item.volume
		PackageLine.volumeMeasureUnit = 'CO'
		PackageLines.push(PackageLine)
	}

	const ConsignmentProcess = {
		consignment: {
			consignmentNumber: source.consignmentNumber,
			payingAccount: source.spAccountDetails.accountKey,
			collection: tntLabelAddressMapper.map(source.pickupAddress),
			delivery: tntLabelAddressMapper.map(source.dropAddress),
			consignmentDate: source.consignmentDate,
			serviceCode: source.serviceType,
			dangerousGoodsIndicator: 'N',
			payer: 'S',
			foodIndicator: 'N',
			cancelIndicator: 'N',
			extendedWarrantyValue: 2000,
			consignmentFinaliseIndicator: 'N',

			consignmentSpecialInstructionList: {
				ConsignmentSpecialInstruction: {
					textType: 'DELIVERY',
					textLine: source.collectionInstructions
				}
			},

			packageLineList: {
				PackageLine: PackageLines
			}
		},

		consignmentActionList: { ConsignmentActionType: 'GETLABEL' },
		senderCode: source.spAccountDetails.accountState,
		labelType: 'A'
	}

	ConsignmentProcessList.ConsignmentProcess = ConsignmentProcess
	return ConsignmentProcessList
}
