exports.map = source => {
	let destination = {}
	destination.weight = parseFloat(source.weight)

	const unitAttr = {
		unit: 'kg'
	}
	destination['@'] = unitAttr

	return destination
}
