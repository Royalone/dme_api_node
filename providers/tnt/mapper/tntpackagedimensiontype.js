exports.map = source => {
	let destination = {}
	destination.length = parseInt(source.length)
	destination.width = parseInt(source.width)
	destination.height = parseInt(source.height)
	const unitAttr = {
		unit: 'cm'
	}
	destination['@'] = unitAttr
	return destination
}
