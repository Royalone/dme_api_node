let objectMapper = require('object-mapper')
const tntTrackStatusMapper = require('./tnttrackstatus')

let map = {
	conNumber: 'consignmentNumber',
	conRefNumber: 'consignmentReferenceNumber',
	signatory: 'signatory',
	status: {
		key: 'consignmentStatuses',
		transform: function (items) {
			let consignmentStatusList = []
			for (const item of items) {
				consignmentStatusList.push(tntTrackStatusMapper.map(item))
			}
			return consignmentStatusList
		}
	}
}

exports.map = source => {
	return objectMapper(source, map)
}
