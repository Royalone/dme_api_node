let objectMapper = require('object-mapper')
const moment = require('moment')

let map = {
	product: [
		{
			key: 'rateCode',
			transform: function (value) {
				return value.code
			}
		},
		{
			key: 'serviceType',
			transform: function (value) {
				return value.description
			}
		}],
	quote: {
		key: 'netPrice',
		transform: function (value) {
			return value.price
		}
	}
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.etd = moment( source.estimatedDeliveryDateTime, 'YYYY-MM-DDTHH:mm:ss').diff( moment(source.readyDate), 'days', true).toFixed(2)
	return destination
}
