let objectMapper = require('object-mapper')

let map = {
	companyName: 'CompanyName',
	contact: 'ContactName',
	contactPhoneAreaCode: 'ContactPhoneAreaCode',
	phoneNumber: 'ContactPhoneNumber',
	'postalAddress.address1': 'AddressLine1',
	'postalAddress.address2': 'AddressLine2',
	'postalAddress.suburb': 'Suburb',
	'postalAddress.state': 'State',
	'postalAddress.postCode': 'Postcode'
}

exports.map = source => {
	return objectMapper(source, map)
}
