let objectMapper = require('object-mapper')

let map = {
	OrderNumber: 'consignmentNumber'
}

exports.map = source => {
	return objectMapper(source, map)
}
