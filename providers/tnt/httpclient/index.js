const axios = require('axios')
const js2xmlparser = require('js2xmlparser')
const soap = require('soap')
const wsdlTNTPodLocation = 'https://www.tntexpress.com.au/webservices/podservice/DeliveryImage.svc?wsdl'
const wsdlTNTBookingLocation = 'http://www.tntexpress.com.au/webservices/booking.svc?wsdl'
const wsdlTNTGetLabelLocation = 'https://www.tntexpress.com.au/Webservices/Conservice/ConsignmentService.svc?wsdl'
const qs = require('querystring')
const logger = require('logger')
const o2x = require('object-to-xml')

let tntPodWsClient = null
let tntBookingWsClient = null
let tntLabellingClient = null

exports.getTNTPodWsClient = async () => {
	let client = await soap.createClientAsync(wsdlTNTPodLocation)
	logger.info('### SUCCESS - TNT POD client Initialized')
	return client
}

exports.getTNTBookingWsClient = async () => {
	let client = await soap.createClientAsync(wsdlTNTBookingLocation)
	logger.info('### SUCCESS - TNT Booking client Initialized')
	return client
}

exports.getTNTLabellingWsClient = async () => {
	let client = await soap.createClientAsync(wsdlTNTGetLabelLocation)
	logger.info('### SUCCESS - TNT Label client Initialized')
	return client
}

exports.getConsigmentEventHistory = consignmentNos => {
	const headers = {
		'Content-Type': 'application/json',
		Accept: 'application/json'
	}

	let consignmentParam = consignmentNos.join()

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_TNT_TRACKING + '?' + consignmentParam
	}
	logger.info('TNT Tracking XML: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(process.env.SERVICE_FP_TNT_TRACKING + '?' + consignmentParam, {
			headers: headers
		}).then(response => {
			logger.info('TNT Tracking Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('TNT Tracking Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getRatedTransitType = (username, password, tntPricingRequest) => {
	const headers = {
		'Content-Type': 'application/x-www-form-urlencoded'
	}

	let encodedXmlRequest = this.urlEncodedXmlRequest(tntPricingRequest)

	const params = {
		username: username,
		password: password,
		version: '2',
		xmlRequest: encodedXmlRequest,
		url: process.env.SERVICE_FP_TNT_PRICING
	}

	logger.info('TNT Pricing XML: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.post(process.env.SERVICE_FP_TNT_PRICING, qs.stringify(params), {
			headers: headers
		}).then(response => {
			logger.info('TNT Pricing Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('TNT Pricing Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.urlEncodedXmlRequest = (tntPricingRequest) => {
	let enquiry = js2xmlparser.parse('enquiry', tntPricingRequest)
	return enquiry
}

exports.retrieveDeliveryImage = async (request) => {
	// 068594
	let imageRetrievalResult = null

	try {
		request.url = wsdlTNTPodLocation
		logger.info("TNT POD XML: " + o2x(request))
		imageRetrievalResult = await tntPodWsClient.RetrieveDeliveryImageAsync({ request })
		imageRetrievalResult = imageRetrievalResult[0].RetrieveDeliveryImageResult

		logger.info('TNT POD Success: ' + JSON.stringify(imageRetrievalResult, null, 4))

	} catch (error) {
		logger.info('TNT POD Failed: ' + JSON.stringify(error, null, 4))
		throw error.message
	}
	return imageRetrievalResult
}

exports.submitBookingRequest = async (userName, password, booking) => {
	let request = { userName, password, booking }
	let bookingResponse = null

	if (!tntBookingWsClient) {
		logger.info('#ERROR  - Connection is not established!')
	} else {
		try {
			request.url = wsdlTNTBookingLocation
			logger.info('TNT Booking XML: ' + o2x(request))
			bookingResponse = await tntBookingWsClient.SubmitBookingRequestAsync(request)

			bookingResponse = bookingResponse[0].SubmitBookingRequestResult
			logger.info('TNT Booking Success: ' + JSON.stringify(bookingResponse, null, 4))
		} catch (error) {
			logger.info('TNT Booking Failed: ' + JSON.stringify(error.message, null, 4))
			throw error
		}
	}

	return bookingResponse
}

exports.getLabel = async (UserName, Password, ConsignmentProcessList) => {
	let Request = { UserName, Password, ConsignmentProcessList }

	let labelResponse = null
	if (!tntLabellingClient) {
		logger.info('### ERROR - Connection is not established!')
	} else {
		try {
			Request.url = wsdlTNTGetLabelLocation

			logger.info('TNT GetLabel XML: ' + o2x(Request))

			labelResponse = await tntLabellingClient.ProcessConsignmentRequestAsync({ Request })
			labelResponse = labelResponse[0].ProcessConsignmentRequestResult

			logger.info('TNT GetLabel Success: ' + JSON.stringify(labelResponse, null, 4))
		} catch (error) {
			logger.info('TNT GetLabel Failed: ' + JSON.stringify(error.message, null, 4))
			throw error.message
		}
	}

	return labelResponse
}

exports.reprintLabel = async (UserName, Password, ConsignmentNumber, labelType) => {
	let Request = {
		UserName, Password, ConsignmentNumber, labelType
	}
	let reprintLabelResponse

	if (!tntLabellingClient) {
		logger.info('### ERROR - Connection is not established!')
	} else {
		try {
			Request.url = wsdlTNTGetLabelLocation
			logger.info('TNT Reprint XML: ' + o2x(Request))
			reprintLabelResponse = await tntLabellingClient.ReprintLabelRequestAsync({ Request })

			reprintLabelResponse = reprintLabelResponse[0].ReprintLabelRequestResult

			logger.info('TNT Reprint Success: ' + JSON.stringify(reprintLabelResponse, null, 4))
		} catch (error) {
			logger.info('TNT Reprint Failed: ' + JSON.stringify(error.message, null, 4))
			throw error.message
		}
	}

	return reprintLabelResponse
}

(async () => {
	tntBookingWsClient = await this.getTNTBookingWsClient()
	tntLabellingClient = await this.getTNTLabellingWsClient()
	tntPodWsClient = await this.getTNTPodWsClient()
	logger.info('### Success - TNT initialized')
})()
