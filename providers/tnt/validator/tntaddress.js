const logger = require('logger')

const AustralianState = [
	'ACT',
	'NSW',
	'NT',
	'QLD',
	'SA',
	'TAS',
	'VIC',
	'WA'
]

exports.validateRequest = (request) => {
	const { postalAddress, contact } = request
	let validated = true

	if (!postalAddress.state || postalAddress.state === '') {
		validated = false
		logger.info(`#108 TNT: State cannot be null or empty in any postal address details for TNT requests. Valid values for state in request = ${AustralianState.join()}`)
	}
	
	if (AustralianState.indexOf(postalAddress.state) < 0) {
		validated = false
		logger.info(`#109 TNT: Valid values for state in request = ${AustralianState.join()}`)
	}

	if (contact.length == 0 || contact.length > 19) {
		validated = false
		logger.info('#110 TNT: Address.ContactName must be between 0 and 20 characters.')
	}

	return validated
}
