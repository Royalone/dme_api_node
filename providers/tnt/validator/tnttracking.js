const logger = require('logger')

exports.validateRequest = (req, reject) => {
	const { consignmentDetails } = req.body
	let validated = true

	try {
		for (const consignmentDetail of consignmentDetails) {
			if (!consignmentDetail.consignmentNumber || consignmentDetail.consignmentNumber === '') {
				validated = false
				logger.info('#111 TNT: Consignment number cannot be empty in any consignment details in the TNT tracking service request.')
				throw new Error('Consignment number cannot be empty in any consignment details in the TNT tracking service request')
			}
		}
	} catch (error) {
		reject({ errorMessage: error.message })
	}

	return validated
}
