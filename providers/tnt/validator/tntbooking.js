const tntAddressValidator = require('./tntaddress')

exports.validateRequest = (req, reject) => {
	const { pickupAddress, dropAddress, items } = req.body
	let validated = true

	try {
		if (!tntAddressValidator.validateRequest(pickupAddress)) return false
		if (!tntAddressValidator.validateRequest(dropAddress)) return false
	} catch (error) {
		reject({ errorMessage: error.message })
	}

	return validated
}
