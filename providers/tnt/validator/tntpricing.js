const logger = require('logger')
const lodash = require('lodash')

const tntAddressValidator = require('./tntaddress')

exports.validateRequest = (req, reject) => {
	const { pickupAddress, dropAddress, items } = req.body
	let validated = true

	if (!tntAddressValidator.validateRequest(pickupAddress)) return false
	if (!tntAddressValidator.validateRequest(dropAddress)) return false

	try {
		if (items.length === 0) {
			validated = false
			logger.info('#105 TNT: Item is required. Please configure only item in the items list to get price for TNT service provider')
		}

		let packageTypes = []
		for (const item of items) {
			packageTypes.push(item.packagingType)
		}

		if (lodash.uniq(packageTypes).length > 1) {
			validated = false
			logger.info('#106 TNT: Only one packagingType value is valid across all items in pricing request for TNT service provider')
		}
	} catch (error) {
		logger.info('#107: ' + error.message)
		reject({ errorMessage: error.message })
	}

	return validated
}
