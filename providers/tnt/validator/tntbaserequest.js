const logger = require('logger')

exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountCode || spAccountDetails.accountCode === '') {
			validated = false
			logger.info('#100 TNT: Service provider account code cannot be null or empty in the request. Request must contain valid TNT account number')
		}

		if (!spAccountDetails.accountPassword || spAccountDetails.accountPassword === '') {
			validated = false
			logger.info('#101 TNT: Service provider password cannot be null or empty in the request. Request must contain valid user password')
		}

		if (!spAccountDetails.accountUsername || spAccountDetails.accountUsername === '') {
			validated = false
			logger.info('#102 TNT: Service provider username cannot be null or empty in the request. Request must contain valid TNT provided username')
		}
	} catch (error) {
		logger.info('#104' + error.message)
		reject({ errorMessage: error.message })
	}

	return validated
}
