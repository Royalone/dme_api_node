const xml2js = require('xml2js')
const parser = new xml2js.Parser({ attrkey: 'ATTR', explicitArray: false })
const moment = require('moment')
const logger = require('logger')

const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const tntPricingValidator = require('../validator/tntpricing')
const tntTransitStopTypeAddress = require('../mapper/tnttransitstopaddresstype')
const tntPackageInfoPackageLineTypeMapper = require('../mapper/tntpackageinfopackagelinetype')
const tntRatedProductTypeConsignmentPriceMapper = require('../mapper/tntratedproducttypeconsignmentprice')

/**
 *
 * @param {*} req
 */
exports.getPriceDetail = (req) => {
	const self = this
	return new Promise((resolve, reject) => {
		const { accountUsername, accountPassword } = req.body.spAccountDetails

		if (!tntBaseRequestValidator.validateRequest(req, reject) ||
			!tntPricingValidator.validateRequest(req, reject)
		) {
			throw new Error(`Request is not validated!`)
		}

		tntHttpClient.getRatedTransitType(accountUsername, accountPassword, self.convertPricingRequestToTNTRequest(req))
			.then(tntPricingResponse => {
				parser.parseString(tntPricingResponse, function (error, result) {
					if (error === null) {
						resolve(self.convertTNTpricingResponseToDMEresponse(req.body.readyDate, result.response))
					} else {
						reject(error)
					}
				})
			}).catch(error => {
				reject(error)
			})
	})
}

exports.convertPricingRequestToTNTRequest = req => {
	let cutOffTimeEnquiryType = {}
	const { pickupAddress, dropAddress, readyDate, items, spAccountDetails } = req.body

	cutOffTimeEnquiryType.collectionAddress = tntTransitStopTypeAddress.map(pickupAddress)
	cutOffTimeEnquiryType.deliveryAddress = tntTransitStopTypeAddress.map(dropAddress)
	cutOffTimeEnquiryType.shippingDate = readyDate
	cutOffTimeEnquiryType.userCurrentLocalDateTime = moment().format('YYYY-MM-DDTHH:mm:ss')

	let dangerousGoodstype = {}
	dangerousGoodstype.dangerous = items[0].dangerous

	cutOffTimeEnquiryType.dangerousGoods = dangerousGoodstype

	let packageLines = {}
	let packageLine = []
	for (const item of items) {
		packageLine.push(tntPackageInfoPackageLineTypeMapper.map(item))
	}
	packageLines.packageLine = packageLine

	const packageTypeAttr = {
		packageType: items[0].packagingType
	}
	packageLines['@'] = packageTypeAttr

	cutOffTimeEnquiryType.packageLines = packageLines

	let rttEnquiryType = {}
	rttEnquiryType.cutOffTimeEnquiry = cutOffTimeEnquiryType

	let termsOfPaymentType = {}
	termsOfPaymentType.senderAccount = spAccountDetails.accountCode
	termsOfPaymentType.payer = 'S'

	rttEnquiryType.termsOfPayment = termsOfPaymentType

	let enquiry = {}
	enquiry.ratedTransitTimeEnquiry = rttEnquiryType

	const enquiryAttr = {
		xmlns: 'http://www.tntexpress.com.au'
	}
	enquiry['@'] = enquiryAttr

	// createEnquiry
	return enquiry
}

exports.convertTNTpricingResponseToDMEresponse =  (readyDate, tntPricingResponse) => {
	let pricingResponse = {}

	let rttResponseType = tntPricingResponse.ratedTransitTimeResponse
	if (rttResponseType) {
		if (!rttResponseType.ratedProducts) { 
			pricingResponse.price = null 

			if (rttResponseType.brokenRules) {
				console.log(rttResponseType.brokenRules)
				console.log(rttResponseType.brokenRules.brokenRule)


				let consignmentErrors = []

				for (let rule of rttResponseType.brokenRules.brokenRule) {
					consignmentErrors.push({
						errorCode: rule.code,
						errorMsg: rule.description
					})
				}

				console.log(consignmentErrors)

				pricingResponse.errors = consignmentErrors
			}
		} 
		else {
			const ratedProducts = rttResponseType.ratedProducts.ratedProduct
			try {
				const prices = []
				for (const ratedProduct of ratedProducts) {
					ratedProduct.readyDate = readyDate
					prices.push(tntRatedProductTypeConsignmentPriceMapper.map(ratedProduct))
				}
				pricingResponse.price = prices
			} catch (error) {
			}
		}
	}

	if (tntPricingResponse.error) {
		pricingResponse.errors = []
		let consignmentErrors = []
		consignmentErrors.push({
			errorCode: tntPricingResponse.error.code,
			errorMsg: tntPricingResponse.error.description
		})

		pricingResponse.errors = consignmentErrors
	}

	return pricingResponse
}
