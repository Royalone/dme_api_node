const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const tntTrackingValidator = require('../validator/tnttracking')
const tntTrackResponseMapper = require('../mapper/tnttrackresponse')
const xml2js = require('xml2js')
const parser = new xml2js.Parser({ attrkey: 'ATTR' })

/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise(function (resolve, reject) {
		if (!tntBaseRequestValidator.validateRequest(req, reject) || !tntTrackingValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails } = req.body

		let consignmentNumbers = []
		for (let consignmentDetail of consignmentDetails) {
			consignmentNumbers.push(consignmentDetail.consignmentNumber)
		}

		tntHttpClient.getConsigmentEventHistory(consignmentNumbers)
			.then(eventList => {
				let trackResponse = {}
				let consignmentTrackDetailList = []

				let correctEventList = eventList.replace("'", "`");
				parser.parseString(correctEventList, function (error, result) {
					if (error === null) {
						if(result.error) {
							throw Error(result.error.message[0]);
						}

						for (const statusHistoryOrMissing of result.statusHistoryList.statusHistory) {
							consignmentTrackDetailList.push(tntTrackResponseMapper.map(statusHistoryOrMissing))
						}

						trackResponse.consignmentTrackDetails = consignmentTrackDetailList
						resolve(trackResponse)
					}
				})
			}).catch(error => {
				reject({errorMessage: error.message})
			})
	})
}
