const tntBookingRequestMapper = require('../mapper/tntbookingrequest')
const tntBookingResponseMapper = require('../mapper/tntbookingresponse')
const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const tntBookingValidator = require('../validator/tntbooking')

/**
 *
 * @param {*} req
 */
exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!tntBaseRequestValidator.validateRequest(req, reject) || !tntBookingValidator.validateRequest(req, reject)) {
			return
		}

		try {
			let tntBookingRequest = tntBookingRequestMapper.map(req.body)
			const { accountUsername, accountPassword } = req.body.spAccountDetails
			let response = await tntHttpClient.submitBookingRequest(accountUsername, accountPassword, tntBookingRequest)

			if ( response.Errors) {
				throw Error(response.Errors.ErrorInformation.ErrorMessage);
			}
			
			resolve(tntBookingResponseMapper.map(response))
		} catch (error) {
			reject({errorMessage: error.message})
		}
	})
}

/**
 *
 * @param {*} req
 */
exports.rebookConsignmentWithFreightProvider = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!tntBaseRequestValidator.validateRequest(req, reject) || !tntBookingValidator.validateRequest(req, reject)) {
			return
		}

		try {
			let tntBookingRequest = tntBookingRequestMapper.map(req.body)
			const { accountUsername, accountPassword } = req.body.spAccountDetails
			let response = await tntHttpClient.submitBookingRequest(accountUsername, accountPassword, tntBookingRequest)

			if ( response.Errors) {
				throw Error(response.Errors.ErrorInformation.ErrorMessage);
			}

			resolve(tntBookingResponseMapper.map(response))
		} catch (error) {
			reject({errorMessage: error.message})
		}
	})
}

