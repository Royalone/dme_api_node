const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const logger = require('logger')
/**
 *
 * @param {*} req
 */
exports.reprintLabel = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!tntBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		try {
			const { accountUsername, accountPassword } = req.body.spAccountDetails
			const { consignmentNumber } = req.body
			const { labelType } = req.body
			let response = await tntHttpClient.reprintLabel(accountUsername, accountPassword, consignmentNumber, labelType)
			

			if ( response.Error) {
				throw Error(response.Error.Message);
			}

			let responseBody = Object.assign({}, response)

			responseBody["ReprintActionResult"]["LabelPDF"] = responseBody["ReprintActionResult"]["LabelPDF"].slice(0, 10) + "..."
			logger.info('TNT Reprint Response: ' + JSON.stringify(responseBody, null, 4))

			resolve(response)
		} catch (error) {
			reject({errorMessage: error.message})
		}
	})
}
