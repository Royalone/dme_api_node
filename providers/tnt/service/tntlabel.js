const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const tntLabelRequestMapper = require('../mapper/tntlabelrequest')
const logger = require('logger')
/**
 *
 * @param {*} req
 */
exports.getFreightProviderLabel = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!tntBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		try {
			let tntLabelRequest = tntLabelRequestMapper.map(req.body)
			const { accountUsername, accountPassword } = req.body.spAccountDetails
			let response = await tntHttpClient.getLabel(accountUsername, accountPassword, tntLabelRequest)

			if ( response.Error) {
				throw Error(response.Error.Message);
			}

			let result = response.ConsignmentProcessResultList.ConsignmentProcessResult.ConsignmentActionList
			let labelResponse = JSON.parse(JSON.stringify(result))
			labelResponse["anyType"]["LabelPDF"] = labelResponse["anyType"]["LabelPDF"].slice(0, 10) + "..."
			logger.info('TNT GetLabel Response: ' + JSON.stringify(labelResponse, null, 4))
			resolve(result)
		} catch (error) {
			reject({errorMessage: error.message})
		}
	})
}
