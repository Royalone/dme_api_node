const tntHttpClient = require('../httpclient')
const tntBaseRequestValidator = require('../validator/tntbaserequest')
const logger = require('logger')
/**
 *
 * @param {*} req
 */
exports.fetchProofOfdelivery = (req) => {
	const self = this
	return new Promise(async (resolve, reject) => {
		if (!tntBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		try {
			const { consignmentDetails } = req.body
			const { accountUsername, accountPassword } = req.body.spAccountDetails

			let imageRetrievalRequest = {}
			imageRetrievalRequest.ConsignmentNumber = consignmentDetails.consignmentNumber
			imageRetrievalRequest.Password = accountPassword
			imageRetrievalRequest.UserName = accountUsername

			let imageRetrievalResult = await tntHttpClient.retrieveDeliveryImage(imageRetrievalRequest)


			if (imageRetrievalResult.Errors) {
				throw Error(imageRetrievalResult.Errors.ErrorResponse.Message)
			}

			let consignmentPodResponse = self.createConsignmentPodResponse(imageRetrievalResult)

			let consignmentPodResponseData = JSON.parse(JSON.stringify(consignmentPodResponse))
			consignmentPodResponseData.pod.podData = consignmentPodResponseData.pod.podData.slice(0, 10) + "..."
			logger.info('TNT Pod Response: ' + JSON.stringify(consignmentPodResponseData, null, 4))
			resolve(consignmentPodResponse)
		} catch (error) {
			reject({errorMessage: error.message})
		}
	})
}

exports.createConsignmentPodResponse = imageRetrievalResult => {
	let consignmentPodResponse = {}
	if (!imageRetrievalResult.DeliveryInformation) { consignmentPodResponse.pod = null } else { consignmentPodResponse.pod = this.createConsignmentPodResults(imageRetrievalResult.DeliveryInformation) }

	return consignmentPodResponse
}

exports.createConsignmentPodResults = deliveryInfo => {
	if (deliveryInfo == null) { return null }

	let consignmentPodResult = {}

	if (deliveryInfo.ImageDetailsList) {
		const imageDetails = deliveryInfo.ImageDetailsList.ImageDetails
		consignmentPodResult.consignmentNumber = deliveryInfo.ConsignmentNumber
		consignmentPodResult.deliveryImageType = imageDetails.DeliveryImageType
		consignmentPodResult.imageContentType = imageDetails.ImageContentType
		consignmentPodResult.podData = imageDetails.ImageData
		consignmentPodResult.signedBy = imageDetails.SignedBy
	} else {
		consignmentPodResult.consignmentNumber = deliveryInfo.ConsignmentNumber
	}

	return consignmentPodResult
}