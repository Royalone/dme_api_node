const dpdpostaladdressMapper = require('./dpdpostaladdress')
const objectMapper = require('object-mapper')
const moment = require('moment')

let map = {

}

exports.map = source => {
	let consignments = []
	let consignment = {}
	consignment.networkCode = '1^12'
	consignment.numberOfParcels = source.items.length
	consignment.shippingRef1 = 'My Ref 1'
	consignment.shippingRef2 = 'My Ref 2'
	consignment.shippingRef3 = 'My Ref 3'
	consignment.deliveryInstructions = 'Please deliver with neighbour'
	consignment.collectionDetails = dpdpostaladdressMapper.map(source.pickupAddress)
	consignment.deliveryDetails = dpdpostaladdressMapper.map(source.dropAddress)
	consignment.parcel = source.items
	consignments.push(consignment)

	let destination = objectMapper(source, map)
	destination.consolidate = moment().format('YYYY-MM-DDTHH:mm:ss')
	destination.consignment = consignments
	return destination
}
