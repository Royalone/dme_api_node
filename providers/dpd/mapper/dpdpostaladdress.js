const objectMapper = require('object-mapper')

let map = {
	contact: 'name',
	width: 'width',
	emailAddress: 'email',
	phoneNumber: 'phone',
	postalAddress: [
		{
			key: 'suburb',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'state',
			transform: function (value) {
				return value.state
			}
		},
		{
			key: 'postcode',
			transform: function (value) {
				return value.postCode
			}
		},
		{
			key: 'lines',
			transform: function (value) {
				let arr = []
				arr.push(value.address1)
				arr.push(value.address2)
				return arr
			}
		}
	]
}

exports.map = source => {
	return objectMapper(source, map)
}
