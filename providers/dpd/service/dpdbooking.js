const dpdHttpClient = require('../httpclient')
const dpdBaseRequestValidator = require('../validator/stbaserequest')

/**
 *
 * @param {*} req
 */
exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(function (resolve, reject) {
		if (!dpdBaseRequestValidator.validateRequest(req, reject) || !dpdBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { spAccountDetails } = req.body
		dpdHttpClient.bookShipments(req.body, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}
