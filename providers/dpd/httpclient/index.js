const Base64 = require('base64util')
const axios = require('axios')
const logger = require('logger')

exports.createAuthHeaders = (accountNumber, accountKey, accountPasswd) => {
	return {
		'Content-Type': 'application/json',
		GeoClient: `account/${accountNumber}`,
		Authorization: this.createAuthorizationApiKey(accountKey, accountPasswd)
	}
}

exports.createAuthorizationApiKey = (accountKey, accountPasswd) => {
	return 'Basic ' + Base64.encode(accountKey + ':' + accountPasswd)
}

exports.loginGeoClient = (accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_DPD_LOGIN,
		method: 'post'
	}
	logger.info('DPD Login Params: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('DPD Login Response: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			reject(error.response.data.errors)
		})
	})
}

exports.bookShipments = (bookingRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_DPD_BOOKING,
		data: bookingRequest,
		method: 'post'
	}
	logger.info('DPD Booking Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('DPD Booking Response: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			reject(error.response.data.errors)
		})
	})
}

this.loginGeoClient(830567, 'edidev', 'acy7bies')
