const bexHttpClient = require('../httpclient')

/**
 *
 * @param {*} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise(function (resolve, reject) {
		const { consignmentDetails } = req.body
		let consignmentTrackDetails = []

		if (consignmentDetails.length === 0) {
			resolve({})
		}

		for (const consignmentDetail of consignmentDetails) {
			let consTrackDetails = { consignmentStatuses: [] }

			bexHttpClient.trackShipping(consignmentDetail.consignmentNumber)
				.then(response => {
					for (const bexResponse of response.data) {
						consTrackDetails.consignmentStatuses.push({
							description: bexResponse.Description,
							code: bexResponse.Code,
							comment: bexResponse.Comment,
							statusDate: bexResponse.DateOccurred
						})

						consTrackDetails.consignmentNumber = bexResponse.ConnoteNumber
					}

					consignmentTrackDetails.push(consTrackDetails)

					if (consignmentDetails.length === consignmentTrackDetails.length) {
						resolve({
							consignmentTrackDetails: consignmentTrackDetails
						})
					}
				}).catch(error => {
					reject(error)
				})
		}
	})
}
