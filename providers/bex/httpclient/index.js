const axios = require('axios')
const logger = require('logger')

exports.createAuthHeaders = (accountKey) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: this.createAuthorizationApiKey(accountKey)
	}
}

exports.createAuthorizationApiKey = (accountKey) => {
	return 'Basic ' + accountKey
}

exports.trackShipping = (consignmentNumber) => {
	return new Promise(function (resolve, reject) {
		logger.info('Bex Tracking URL: ' + process.env.SERVICE_FP_ST_TRACKING + '/bex/api/connote/' + consignmentNumber + '/Events');
		axios.get(process.env.SERVICE_FP_ST_TRACKING + '/bex/api/connote/' + consignmentNumber + '/Events', {
		}).then(response => {
			resolve(response.data)
		}).catch(error => {
			reject(error.response.data.errors)
		})
	})
}
