const axios = require('axios')
const logger = require('logger')
const unirest = require('unirest')

exports.listRFS = (accountKey) => {
	const request = {
		CountryCode: 1, 
		api_key: accountKey
	}
	return new Promise(function (resolve, reject) {
		unirest.get(process.env.SERVICE_FP_FASTWAY_LISTRFS)
			.strictSSL(false)
			.query(request)
			.then(response => {
				if (response.code >= 300) {
					let error = response.body
					let errorMessage = error.errorMessage
					errorMessage = errorMessage.replace('\'', '')
					error.errorMessage = errorMessage
					reject(error)
				} else { 
					logger.info('Fastway LISTRFS Response: ' + JSON.stringify(response.body.result))
					resolve(response.body.result) 
				}
			}).catch(error => {
				let errorMessage = error.errorMessage
				errorMessage = errorMessage.replace('\'', '')
				error.errorMessage = errorMessage
				reject(error)
			})

	})
}


exports.calculatePrice = (pricingRequest) => {
	// pricingRequest.RFCode = 'SYD'
	const params = {
		url: process.env.SERVICE_FP_FASTWAY_PRICING,
		data: pricingRequest,
		method: 'get'
	}
	logger.info('Fastway Pricing Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		unirest.get(process.env.SERVICE_FP_FASTWAY_PRICING)
			.strictSSL(false)
			.query(pricingRequest)
			.then(response => {
				if (response.code >= 300) {
					let error = response.body
					let errorMessage = error.errorMessage
					errorMessage = errorMessage.replace('\'', '')
					error.errorMessage = errorMessage
					reject(error)
				} else { 
					logger.info('Fastway Pricing Response: ' + JSON.stringify(response.body, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				let errorMessage = error.errorMessage
				errorMessage = errorMessage.replace('\'', '')
				error.errorMessage = errorMessage
				reject(error)
			})

	})
}


exports.trackShipping = (consignmentNo, accountKey) => {
	const params = {
		url: process.env.SERVICE_FP_FASTWAY_TRACKING,
		data: {
			LabelNo: consignmentNo,
			api_key: accountKey
		},
		method: 'get'
	}
	logger.info('Fastway Tracking Params: ' + JSON.stringify(params, null, 4))
	const request = {
		LabelNo: consignmentNo,
		api_key: accountKey
	}
	return new Promise(function (resolve, reject) {
		unirest.get(process.env.SERVICE_FP_FASTWAY_TRACKING)
			.strictSSL(false)
			.query(request)
			.then(response => {
				if (response.code >= 300) {
					let error = response.body
					let errorMessage = error.errorMessage
					errorMessage = errorMessage.replace('\'', '')
					error.errorMessage = errorMessage
					reject(error)
				} else {
					logger.info('Fastway Tracking Response: ' + JSON.stringify(response.body, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				let errorMessage = error.errorMessage
				errorMessage = errorMessage.replace('\'', '')
				error.errorMessage = errorMessage
				reject(error)
			})

	})
}