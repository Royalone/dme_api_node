
let objectMapper = require('object-mapper')
let _ = require('lodash')

let map = {
	dropAddress: [
		{
			key: 'Suburb',
			transform: function (value) {
				return value.postalAddress.suburb
			}
		},
		{
			key: 'DestPostcode',
			transform: function (value) {
				return value.postalAddress.postCode
			}
		}
	],
	spAccountDetails: [
		{
			key: 'api_key',
			transform: function (value) {
				return value.accountKey
			}
		}
	],
	items: [
		{
			key: 'WeightInKg',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.weight
				})
			}
		},
		{
			key: 'LengthInCm',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.length
				})
			}
		},
		{
			key: 'WidthInCm',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.width
				})
			}
		},
		{
			key: 'HeightInCm',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.height
				})
			}
		}
	]
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.AllowMultipleRegions = true
	return destination
}
