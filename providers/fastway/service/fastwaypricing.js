const fastwayPricingRequestMapper = require('../mapper/fastwaypricingrequest')
const fastwayHttpClient = require('../httpclient')
const fastwayBaseRequestValidator = require('../validator/fastwaybaserequest')

/**
 *
 * @param {*} req
 */
exports.getPriceDetail = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!fastwayBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const request = fastwayPricingRequestMapper.map(req.body)

		const codes = await fastwayHttpClient.listRFS(req.body.spAccountDetails.accountKey)

		if (codes === undefined) {
			reject(new Error('No Codes'))
		}

		const code = codes.find((element) => {
			return element.FranchiseName.toUpperCase() === req.body.pickupAddress.postalAddress.suburb.toUpperCase()
		})

		if (code) {
			request.RFCode = code.FranchiseCode		
		}

		fastwayHttpClient.calculatePrice(request)
			.then(response => {
				resolve({price: response.result, error: response.error})
			}).catch(error => {
				reject(error)
			})
	})
}
