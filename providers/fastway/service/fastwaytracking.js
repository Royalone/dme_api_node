const fastwayPricingRequestMapper = require('../mapper/fastwaypricingrequest')
const fastwayHttpClient = require('../httpclient')
const fastwayBaseRequestValidator = require('../validator/fastwaybaserequest')

/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise((resolve, reject) => {
		if (!fastwayBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails, spAccountDetails } = req.body
		let response = {}
		response.consignmentTrackDetails = []

		if (consignmentDetails.length === 0) {
			resolve(response)
			return
		}

		let consignmentNos = []
		for (let consignmentDetail of consignmentDetails) {
			consignmentNos.push(consignmentDetail.consignmentNumber)
		}

		fastwayHttpClient.trackShipping(consignmentNos.join(), spAccountDetails.accountKey)
			.then(fastwayTrackResponse => {
				resolve(fastwayTrackResponse.result)
			}).catch(error => {
				reject(error)
			})
	})
}
