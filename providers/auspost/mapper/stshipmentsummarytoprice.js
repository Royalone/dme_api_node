const objectMapper = require('object-mapper')

const map = {
	number_of_items: 'chargeQuantity',
	total_cost: 'netPrice',
	freight_charge: 'freightCharge',
	fuel_surcharge: 'fuelSurcharge',
	total_gst: 'totalTaxes'
}

exports.map = source => {
	return objectMapper(source, map)
}
