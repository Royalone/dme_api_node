const stShipmentSummaryToPriceMapper = require('./stshipmentsummarytoprice')
const objectMapper = require('object-mapper')

const map = {
	shipment_summary: {
		key: 'price',
		transform: function (value) {
			return stShipmentSummaryToPriceMapper.map(value)
		}
	},
	shipment_id: 'consignmentNumber',
	shipment_reference: 'consignmentReferenceNumber',
	movement_type: 'status',
	items: 'items'
}

exports.map = source => {
	return objectMapper(source, map)
}
