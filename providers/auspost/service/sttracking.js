const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')

exports.getTrackingDetail = (req) => {
	const self = this
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails, spAccountDetails } = req.body
		let consignmentNos = []
		for (let consignmentDetail of consignmentDetails) {
			consignmentNos.push(consignmentDetail.consignmentNumber)
		}
		stHttpClient.trackShipping(consignmentNos.join(), spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				const consignmentTrackDetails = []
				for (const trackingResult of response.tracking_results) {
					let trackingDetails = {}
					trackingDetails.consignmentNumber = trackingResult.tracking_id
					if (trackingResult.consignment) {
						trackingDetails = self.addConsignmentStatus(trackingDetails, trackingResult.consignment)
						trackingDetails = self.addTransitStops(trackingDetails, trackingResult.consignment.events)
					}

					if (trackingResult.trackable_items) {
						for (const trackableItem of trackingResult.trackable_items) {
							trackingDetails = self.addTransitStops(trackingDetails, trackableItem.events)
						}
					}
					consignmentTrackDetails.push(trackingDetails)
				}

				resolve({
					consignmentTrackDetails: consignmentTrackDetails
				})
			}).catch(error => {
				reject(error)
			})
	})
}

exports.addConsignmentStatus = (consignmentTrackDetails, stConsignment) => {
	let consignmentStatus = {}
	consignmentStatus.status = stConsignment.status
	let consignmentStatuses = []
	consignmentStatuses.push(consignmentStatus)
	consignmentTrackDetails.consignmentStatuses = consignmentStatuses
	return consignmentTrackDetails
}

exports.addTransitStops = (consignmentTrackDetails, stEvents) => {
	if (stEvents != null) {
		if (consignmentTrackDetails.consignmentStops == null) {
			consignmentTrackDetails.consignmentStops = []
		}

		for (const stEvent of stEvents) {
			let transitStop = {}
			transitStop.description = stEvent.description
			transitStop.postalAddress = {}
			transitStop.postalAddress.state = stEvent.location
			transitStop.transitDate = stEvent.date
			consignmentTrackDetails.consignmentStops.push(transitStop)
		}
	}
	return consignmentTrackDetails
}
