const stBookingRequestMapper = require('../mapper/stbookingrequest')
const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')

exports.getPriceDetail = (req) => {
	return new Promise(function (resolve, reject) {
		const stShipmentRequest = stBookingRequestMapper.map(req.body)
		let stShipments = []
		stShipments.push(stShipmentRequest)
		const { spAccountDetails } = req.body
		stHttpClient.calculateShipmentPrice(stShipments, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				const stShipmentSummaryToPriceMapper = require('../mapper/stshipmentsummarytoprice')
				let objectMapper = require('object-mapper')

				let map = {
					shipments: [
						{
							key: 'price',
							transform: function (shipments) {
								let priceList = []
								for (const shipment of shipments) {
									priceList.push(stShipmentSummaryToPriceMapper.map(shipment.shipment_summary))
								}
								return priceList
							}
						},
						{
							key: 'items',
							transform: function (shipments) {
								let itemList = []
								for (const shipment of shipments) {
									itemList = itemList.concat(shipment.items)
								}
								return itemList
							}
						}
					]
				}
				resolve(objectMapper(response, map))
			}).catch(error => {
				reject(error)
			})
	})
}
