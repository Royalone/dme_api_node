/**
 *
 * @param {*} req
 */

const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')
const objectMapper = require('object-mapper')

exports.getAccounts = (req) => {

	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentNumber, spAccountDetails } = req.body

		stHttpClient.getAccounts(spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}