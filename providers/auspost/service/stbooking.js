const stBookingRequestMapper = require('../mapper/stbookingrequest')
const stBookingResponseMapper = require('../mapper/stbookingresponse')
const stHttpClient = require('../httpclient')
const stBaseRequestValidator = require('../validator/stbaserequest')
const stBookingValidator = require('../validator/stbooking')

exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject) || !stBookingValidator.validateRequest(req, reject)) {
			return
		}

		const stShipmentRequest = stBookingRequestMapper.map(req.body)
		let stShipments = []
		stShipments.push(stShipmentRequest)
		const { spAccountDetails } = req.body
		stHttpClient.bookShipments(stShipments, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				resolve(stBookingResponseMapper.map(response.shipments[0]))
			}).catch(ex => {
				let errors = {}

				if (ex.errors) {
					const consignmentErrors = []
					for (const error of ex.errors) {
						const consignmentError = {}
						consignmentError.errorCode = error.code
						consignmentError.errorMsg = error.message.replace('\'', '')
						consignmentError.errorType = error.name
						consignmentErrors.push(consignmentError)
					}
					errors = { errors: consignmentErrors }
				} else {
					errors = { errors: ex }
				}
				reject(errors)
			})
	})
}

exports.updateConsignment = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) { return }

		const { spAccountDetails } = req.body
		const stShipmentRequest = stBookingRequestMapper.map(req.body)
		stHttpClient.updateShipment(stShipmentRequest, stShipmentRequest.consignmentNumber, spAccountDetails.accountCode, spAccountDetails.accountKey, spAccountDetails.accountPassword)
			.then(response => {
				if (response.shipment_id) { resolve({ consignmentNumber: response.shipment_id }) } else { resolve({ consignmentNumber: stShipmentRequest.consignmentNumber }) }
			}).catch(error => {
				reject(error)
			})
	})
}

exports.cancelConsignment = (req) => {
	return new Promise(function (resolve, reject) {
		if (!stBaseRequestValidator.validateRequest(req, reject)) { return }

		const { spAccountDetails } = req.body
		const consignmentIds = req.body.consignmentNumbers

		stHttpClient.cancelShipments(
			consignmentIds,
			spAccountDetails.accountCode,
			spAccountDetails.accountKey,
			spAccountDetails.accountPassword
		)
			.then(response => {
				let bookingCancelResults = []
				for (let consignmentNumber of req.body.consignmentNumbers) {
					bookingCancelResults.push({ consignmentNumber: consignmentNumber, status: 'Cancelled' })
				}

				resolve({ bookingCancelResults: bookingCancelResults })
			}).catch(error => {
				reject(error)
			})
	})
}
