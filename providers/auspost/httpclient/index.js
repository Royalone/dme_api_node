const Base64 = require('base64util')
const axios = require('axios')
const logger = require('logger')

exports.createAuthHeaders = (accountNumber, accountKey, accountPasswd) => {
	return {
		'Content-Type': 'application/json',
		'Accept': 'application/json',
		'AUSPOST-PARTNER-ID': 'DELIVERME-4044',
		'Account-Number': accountNumber,
		'Authorization': this.createAuthorizationApiKey(accountKey, accountPasswd)
	}
}

exports.createAuthorizationApiKey = (accountKey, accountPasswd) => {
	return 'Basic ' + Base64.encode(accountKey + ':' + accountPasswd)
}

exports.bookShipments = (bookingRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_BOOKING,
		data: {
			shipments: bookingRequest
		},
		method: 'post'
	}

	logger.info('AUSPOST Booking Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST Booking Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST Booking Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data)
		})
	})
}

exports.updateShipment = (shipmentRequest, shipmentId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_BOOKING + '/' + shipmentId,
		data: shipmentRequest,
		method: 'put'
	}

	logger.info('AUSPOST EditBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST EditBook Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST EditBook Failed: ' + JSON.stringify(error.response.data.errors, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.cancelShipments = (consignmentIds, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	let stShipmentIds = consignmentIds.join()

	const params = {
		headers: headers,
		params: {
			shipment_ids: stShipmentIds
		},
		method: 'delete'
	}

	logger.info('AUSPOST CancelBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.delete(process.env.SERVICE_FP_AUSPOST_BOOKING, params)
			.then(response => {
				logger.info('AUSPOST CancelBook Success')
				resolve(response.data)
			}).catch(error => {
				console.log('AUSPOST CancelBook Failed', error)
				logger.info('AUSPOST CancelBook Failed')
				reject(error.response.data.errors)
			})
	})
}

exports.calculateShipmentPrice = (stPricingRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_PRICING,
		data: {
			shipments: stPricingRequest
		},
		method: 'post'
	}
	logger.info('AUSPOST Pricing Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST Pricing Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST Pricing Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.createLabel = (createLabelRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_LABELLING_CREATE,
		data: createLabelRequest,
		method: 'post'
	}
	logger.info('AUSPOST CreateLabel Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST CreateLabel Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST CreateLabel Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getLabel = (requestId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_LABELLING_GET + '/' + requestId,
		method: 'get'
	}
	logger.info('AUSPOST GetLabel Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST GetLabel Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST GetLabel Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getOrderSummary = (requestId, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)
	delete headers.Accept

	const stOrderSummaryUrl = process.env.SERVICE_FP_AUSPOST_ORDER_SUMMARY + '/' + accountNumber + '/orders/' + requestId + '/summary'

	const params = {
		responseType: 'arraybuffer',
		headers: headers,
		method: 'get'
	}

	logger.info('AUSPOST GetOrderSummary Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(stOrderSummaryUrl, params).then(response => {

			logger.info('AUSPOST GetOrderSummary Success: ' + response.data)
			resolve({
				orderSummaryPdf: response.data,
				orderId: requestId
			})
		}).catch(error => {
			logger.info('AUSPOST GetOrderSummary Failed: ' + error.response.data)
			reject(error.response.data.errors)
		})
	})
}

exports.createOrder = (orderRequest, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_ORDER_CREATE,
		data: orderRequest,
		method: 'put'
	}

	logger.info('AUSPOST CreateOrder Payload: ' + JSON.stringify(params, null, 4))

	return new Promise((resolve, reject) => {
		axios(params).then(response => {
			logger.info('AUSPOST CreateOrder Success: ')
			resolve(response)
		}).catch(error => {
			logger.info('AUSPOST CreateOrder Failed: ' + error)
			reject(error.response)
		})
	})
}

exports.trackShipping = (trackingNumber, accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		params: {
			tracking_ids: trackingNumber
		},
		method: 'get'
	}

	logger.info('AUSPOST Tracking Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.get(process.env.SERVICE_FP_AUSPOST_TRACKING, params).then(response => {
			logger.info('AUSPOST Tracking Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST Tracking Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.getAccounts = (accountNumber, accountKey, accountPasswd) => {
	const headers = this.createAuthHeaders(accountNumber, accountKey, accountPasswd)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_AUSPOST_GET_ACCOUNTS + '/' + accountNumber,
		method: 'get'
	}
	logger.info('AUSPOST GetAccounts Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('AUSPOST GetAccounts Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('AUSPOST GetAccounts Failed: ' + JSON.stringify(error.response.data, null, 4))
			reject(error.response.data.errors)
		})
	})
}

