const sendlePricingRequestMapper = require('../mapper/sendlepricingrequest')
const sendleHttpClient = require('../httpclient')
const sendleBaseRequestValidator = require('../validator/sendlebaserequest')

exports.getPriceDetail = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!sendleBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const request = sendlePricingRequestMapper.map(req.body)

		sendleHttpClient.calculatePrice(request)
			.then(response => {
				resolve(response)
			})
			.catch(error => {
				reject({"errorMessage": JSON.stringify(error)})
			})
	})
}
