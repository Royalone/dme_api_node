const sendleHttpClient = require('../httpclient')
const sendleBaseRequestValidator = require('../validator/sendlebaserequest')

exports.getTrackingDetail = (req) => {
	return new Promise((resolve, reject) => {
		if (!sendleBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails } = req.body
		let response = {}
		response.consignmentTrackDetails = []

		if (consignmentDetails.length === 0) {
			resolve(response)
			return
		}

		let consignmentNos = []
		for (let consignmentDetail of consignmentDetails) {
			consignmentNos.push(consignmentDetail.consignmentNumber)
		}

		sendleHttpClient.trackShipping(consignmentNos.join())
			.then(sendleTrackResponse => {
				let consignmentTrackDetails = []
				let consignmentTrackDetail = {}
				let consignmentStatuses = []
				let tracking_events = sendleTrackResponse.tracking_events

				for(let tracking_event of tracking_events) {
					let consignmentStatus = {}

					consignmentStatus.status = tracking_event.event_type
					consignmentStatus.statusDescription = tracking_event.description
					consignmentStatus.statusUpdate = tracking_event.scan_time
					consignmentStatuses.push(consignmentStatus)
				}

				consignmentTrackDetail.consignmentStatuses = consignmentStatuses
				consignmentTrackDetails.push(consignmentTrackDetail)
				resolve({consignmentTrackDetails})
			})
			.catch(error => {
				reject({"errorMessage": JSON.stringify(error)})
			})
	})
}
