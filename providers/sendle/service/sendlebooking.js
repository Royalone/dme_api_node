const sendleBookingRequestMapper = require('../mapper/sendlebookingrequest')
const sendleHttpClient = require('../httpclient')
const sendleBaseRequestValidator = require('../validator/sendlebaserequest')
const sendleBookingResponseMapper = require('../mapper/sendlebookingresponse')

exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!sendleBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const request = sendleBookingRequestMapper.map(req.body)

		// const codes = await sendleHttpClient.listRFS(req.body.spAccountDetails.accountKey)
		// const code = codes.find((element) =>  { return element.FranchiseName === req.body.pickupAddress.postalAddress.suburb })

		// if(code) {
		// 	   request.RFCode = code.FranchiseCode		
		// }

		sendleHttpClient.bookShipments(request, req.body.spAccountDetails)
			.then(response => {
				resolve(sendleBookingResponseMapper.map(response))
			})
			.catch(error => {
				reject({"errorMessage": JSON.stringify(error)})
			})
	})
}

exports.cancelConsignment = (req) => {
	return new Promise(function (resolve, reject) {
		if (!sendleBaseRequestValidator.validateRequest(req, reject)) { return }

		const { spAccountDetails } = req.body
		const consignmentIds = req.body.consignmentNumbers

		sendleHttpClient.cancelShipments(consignmentIds.join(), spAccountDetails)
			.then(response => {
				resolve(response)
			})
			.catch(error => {
				reject({"errorMessage": JSON.stringify(error)})
			})
	})
}
