const sendleHttpClient = require('../httpclient')
const sendleBaseRequestValidator = require('../validator/sendlebaserequest')

exports.getFreightProviderLabel = (req) => {
	return new Promise(function (resolve, reject) {
		if (!sendleBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentNumber, spAccountDetails } = req.body

		sendleHttpClient.getLabel(consignmentNumber, spAccountDetails)
			.then(response => {
				resolve(response)
			})
			.catch(error => {
				reject({"errorMessage": JSON.stringify(error)})
			})
	})
}
