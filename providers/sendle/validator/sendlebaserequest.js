exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountKey || spAccountDetails.accountKey === '') {
			validated = false
			throw new Error('Account key cannot be null or empty for Sendle service provider. Please provide valid shared key!')
		}
	} catch (error) {
		reject(error.message)
	}

	return validated
}
