const axios = require('axios')
const logger = require('logger')
const Base64 = require('base64util')

exports.createAuthHeaders = (accountKey, accountPasswd) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: this.createAuthorizationApiKey(accountKey, accountPasswd),
	}
}

exports.createAuthorizationApiKey = (accountKey, accountPasswd) => {
	return 'Basic ' + Base64.encode(accountKey + ':' + accountPasswd)
}

exports.calculatePrice = (pricingRequest) => {
	const params = {
		url: process.env.SERVICE_FP_SENDLE_PRICING,
		data: pricingRequest,
		method: 'get'
	}

	logger.info('SENDLE Pricing Payload: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params)
			.then(response => {
				logger.info('SENDLE Pricing Success: ' + JSON.stringify(response.data, null, 4))
				resolve({"price": response.data})
			}).catch(error => {
				logger.info('SENDLE Pricing Failed: ' + JSON.stringify(error.response.data, null, 4))
				reject(error.response.data)
			})
	})
}

exports.bookShipments = (bookingRequest, spAccountDetails) => {
	const { accountKey, accountPassword } = spAccountDetails

	const headers = this.createAuthHeaders( accountKey, accountPassword )
	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_SENDLE_BOOKING,
		data: bookingRequest,
		method: 'post',
	}

	logger.info('SENDLE BOOK Payoad: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params)
			.then(response => {
				logger.info('SENDLE BOOK Success: ' + JSON.stringify(response.data, null, 4))
				resolve(response.data)
			}).catch(error => {
				logger.info('SENDLE BOOK Failed: ' + JSON.stringify(error.response.data, null, 4))
				reject(error.response.data)
			})
	})
}

exports.trackShipping = (consignmentNo) => {
	const url = `${process.env.SERVICE_FP_SENDLE_TRACKING}/${consignmentNo}`
	const params = {
		url: url,
		method: 'get'
	}

	logger.info('SENDLE Tracking Payload: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params)
			.then(response => {
				logger.info('SENDLE Tracking Response: ' + JSON.stringify(response.data, null, 4))
				resolve(response.data)
			}).catch(error => {
				logger.info('SENDLE Tracking Failed: ' + JSON.stringify(error.response.data, null, 4))
				reject(error.response.data)
			})
	})
}


exports.cancelShipments = (consignmentNo, spAccountDetails) => {
	const { accountKey, accountPassword } = spAccountDetails
	const headers = this.createAuthHeaders( accountKey, accountPassword )

	const url = `${process.env.SERVICE_FP_SENDLE_BOOKING}/${consignmentNo}`
	const params = {
		headers: headers,
		url: url,
		method: 'delete'
	}

	logger.info('SENDLE CancelBook Payload: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params)
			.then(response => {
				logger.info('SENDLE CancelBook Response: ' + JSON.stringify(response.data, null, 4))
				resolve(response.data)
			})
			.catch(error => {
				logger.info('SENDLE CancelBook Failed: ' + JSON.stringify(error.response.data, null, 4))
				reject(error.response.data)
			})
	})
}

exports.getLabel = (consignmentNo, spAccountDetails) => {
	const { accountKey, accountPassword } = spAccountDetails
	const headers = this.createAuthHeaders( accountKey, accountPassword )
	const url = `${process.env.SERVICE_FP_SENDLE_BOOKING}/${consignmentNo}/labels/a4.pdf`
	const params = {
		headers: headers,
		url: url,
		method: 'get',
		responseType: 'stream',
	}
	logger.info('SENDLE GetLabel Params: ' + JSON.stringify(params, null, 4))
	return new Promise(function (resolve, reject) {
		axios(params)
			.then((response) => {
				logger.info('SENDLE GetLabel Response: ' + JSON.stringify({pdfURL:response.data.responseUrl}), null, 4)
				resolve({pdfURL:response.data.responseUrl});
			})
			.catch(error => {
				logger.info('SENDLE GetLabel Failed: ' + error.response.data.statusMessage, null, 4)
				reject(error.response.data.statusMessage)
			})
	})
}
