const objectMapper = require('object-mapper')
const _ = require('lodash')
const moment = require('moment')

const map = {
	pickupAddress: {
		key: 'sender',
		transform: (value) => {
			return {
				contact : {
					name: value.contact,
					phone: value.phoneNumber,
					company: value.companyName
				},
				address: {
					address_line1: value.postalAddress.address1,
					suburb: value.postalAddress.suburb,
					state_name: value.postalAddress.state,
					postcode: value.postalAddress.postCode,
					country: 'Australia',
				},
				instructions: value.instruction
			}
		}
	},
	dropAddress: {
		key: 'receiver',
		transform: (value) => {
			return {
				contact : {
					name: value.contact,
					phone: value.phoneNumber,
					company: value.companyName
				},
				address: {
					address_line1: value.postalAddress.address1,
					suburb: value.postalAddress.suburb,
					state_name: value.postalAddress.state,
					postcode: value.postalAddress.postCode,
					country: 'Australia',
				},
				instructions: value.instruction
			}
		}
	},
	items: [
		{
			key: 'kilogram_weight',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.weight
				})
			}
		}
	]
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.description = "Sendle booking"
	destination.pickup_date = moment(source.readyDate).format('YYYY-MM-DD')
	destination.first_mile_option = 'pickup'
	return destination
}
