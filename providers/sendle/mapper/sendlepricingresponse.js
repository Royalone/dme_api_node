const objectMapper = require('object-mapper')

const map = {
	Reference1: 'consignmentReferenceNumber',
	Reference2: 'consignmentReferenceNumber2',
	StatusCode: 'status',
	State: 'state',
	StatusDescription: 'statusDescription'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.price = {
		netPrice: source.JobTotalPrice,
		totalTaxes: source.Gst,
		jobPriceExGst: source.JobPriceExGst
	}
	return destination
}
