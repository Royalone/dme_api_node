const objectMapper = require('object-mapper')

const map = {
	order_id: 'consignmentNumber',
	sendle_reference: 'v_FPBookingNumber',
	state: 'status',
	route: 'route'
}

exports.map = source => {
	return objectMapper(source, map)
}
