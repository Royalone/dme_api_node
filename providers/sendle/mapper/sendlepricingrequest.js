const objectMapper = require('object-mapper')
const _ = require('lodash')

const map = {
	dropAddress: [
		{
			key: 'delivery_suburb',
			transform: function (value) {
				return value.postalAddress.suburb
			}
		},
		{
			key: 'delivery_postcode',
			transform: function (value) {
				return value.postalAddress.postCode
			}
		}
	],
	pickupAddress: [
		{
			key: 'pickup_suburb',
			transform: function (value) {
				return value.postalAddress.suburb
			}
		},
		{
			key: 'pickup_postcode',
			transform: function (value) {
				return value.postalAddress.postCode
			}
		}
	],
	items: [
		{
			key: 'kilogram_weight',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.weight
				})
			}
		},
		{
			key: 'cubic_metre_volume',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.volume / 10000 // Hardcoded
				})
			}
		}
	]
}

exports.map = source => {
	return objectMapper(source, map)
}
