// HUNTER V2

const hunterHttpClient = require('../httpclient')
const hunterBaseRequestValidator = require('../validator/hunterbaserequest')


exports.createFreightProviderLabel = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}
		const { consignmentDetails, spAccountDetails } = req.body
		hunterHttpClient.uploadLabel(consignmentDetails.consignmentId, consignmentDetails.consignmentNumber, consignmentDetails.base64LabelImage, spAccountDetails.accountKey)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}
