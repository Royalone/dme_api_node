// HUNTER V2

const hunterHttpClient = require('../httpclient')
const hunterBaseRequestValidator = require('../validator/hunterbaserequest')

/**
 *
 * @param {*} req
 */
exports.fetchProofOfdelivery = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}
		const { consignmentDetails, spAccountDetails, jobDate } = req.body
		hunterHttpClient.fetchPod(consignmentDetails.consignmentNumber, jobDate, spAccountDetails.accountCode, spAccountDetails.accountKey)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}


exports.uploadPod = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}
		const { consignmentDetails, spAccountDetails } = req.body
		hunterHttpClient.uploadPod(consignmentDetails.consignmentId, consignmentDetails.consignmentNumber, consignmentDetails.base64PodImage, spAccountDetails.accountKey)
			.then(response => {
				resolve(response)
			}).catch(error => {
				reject(error)
			})
	})
}
