// HUNTER V2

const logger = require('logger')
const hunterHttpClient = require('../httpclient')
const hunterBaseRequestValidator = require('../validator/hunterbaserequest')
const hunterTrackingResponseMapper = require('../mapper/huntertrackingresponse')

/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	return new Promise((resolve, reject) => {
		if (!hunterBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const { consignmentDetails, spAccountDetails } = req.body
		let response = {}
		response.consignmentTrackDetails = []

		if (consignmentDetails.length === 0) {
			resolve(response)
			return
		}

		for (const consignmentDetail of consignmentDetails) {
			hunterHttpClient.getJobStatuses(consignmentDetail.consignmentNumber, spAccountDetails.accountKey)
				.then(hunterTrackResponse => {
					// Convert "'" into "`"
					let trackingDetails = JSON.stringify(hunterTrackResponse)
					trackingDetails = trackingDetails.replace("'", "`")

					const trackingInfo = {
						'status': JSON.parse(trackingDetails)["Data"]["Status"],
						'description': "",
						'statusUpdate': JSON.parse(trackingDetails)["Data"]["StatusDateTime"],
					}
					response.consignmentTrackDetails.push({consignmentStatuses: [trackingInfo]})
					resolve(response)
				}).catch(error => {
					reject(error)
				})
		}
	})
}
