// HUNTER V2

const hunterPackageinfoItemMapper = require('./hunterpackageinfoitem')
const hunterTransitExtMapper = require('./huntertransitext')
let objectMapper = require('object-mapper')

let map = {
	'items': [
		{
			key: 'Rows',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(hunterPackageinfoItemMapper.map(item))
				}
				return newItems
			}
		}
	],
	'spAccountDetails': {
		key: 'CustomerCode',
		transform: function (value) {
			return value.accountCode
		}
	},
	'readyDate' : 'Date',
	'serviceType': 'primaryService',
	'consignmentNoteNumber': 'Number',
	'reference1': 'ConsignmentOtherReferences',
	'reference2': 'ConsignmentOtherReferences2',
	'SenderReference': 'SenderReference',
	'ReceiverReference': 'ReceiverReference',
	'ConsignmentSenderIsResidential': 'ConsignmentSenderIsResidential',
	'ConsignmentReceiverIsResidential': 'ConsignmentReceiverIsResidential',
	'pickupAddress.postalAddress.address1': 'SenderAddress',
	'pickupAddress.postalAddress.address2': 'SenderAddress2',
	'pickupAddress.postalAddress.suburb': 'SenderSuburb',
	'pickupAddress.postalAddress.state': 'SenderState',
	'pickupAddress.postalAddress.postCode': 'SenderPostcode',
	'pickupAddress.companyName': 'SenderName',
	'pickupAddress.contact': 'ConsignmentSenderContact',
	'pickupAddress.phoneNumber': 'ConsignmentSenderPhone',
	'pickupAddress.emailAddress': 'SenderEmail',
	'pickupAddress.instruction': 'ConsignmentPickupSpecialInstructions',
	'SpecialInstructions': 'SpecialInstructions',
	'dropAddress.postalAddress.address1': 'ReceiverAddress',
	'dropAddress.postalAddress.address2': 'ReceiverAddress2',
	'dropAddress.postalAddress.suburb': 'ReceiverSuburb',
	'dropAddress.postalAddress.state': 'ReceiverState',
	'dropAddress.postalAddress.postCode': 'ReceiverPostcode',
	'dropAddress.companyName': 'ReceiverName',
	'dropAddress.contact': 'ConsignmentReceiverContact',
	'dropAddress.phoneNumber': 'ConsignmentReceiverPhone',
	'dropAddress.emailAddress': 'ReceiverEmail',
}

exports.map = source => {
	let destination = objectMapper(source, map)
	return destination
}
