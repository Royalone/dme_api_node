// HUNTER V2

let objectMapper = require('object-mapper')
let _ = require('lodash')

let map = {
	ConsignmentNumber: 'consignmentNumber',
	Id: 'jobNumber',
	jobDate: 'jobDate',
	PdfLabels: 'shippingLabel',
	// ItemScanValues: 'ItemScanValues',
	PdfConsignment: 'podImage'
}

exports.map = source => {
	return objectMapper(source, map)
}
