// HUNTER V2

let objectMapper = require('object-mapper')

let map = {
	depth: 'length',
	pieces: 'quantity'
}

exports.map = source => {
	return objectMapper(source, map)
}
