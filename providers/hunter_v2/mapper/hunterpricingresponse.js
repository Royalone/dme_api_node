// HUNTER V2

const hunterPricingMapper = require('./hunterpricing')

exports.map = source => {
	let newItems = []
	for (let item of source) { newItems.push(hunterPricingMapper.map(item)) }

	let destination = {}
	destination.price = newItems
	return destination
}
