// HUNTER V2

let objectMapper = require('object-mapper')

let map = {
	when: 'statusUpdate',
	info: 'description',
	what: 'status',
	who: 'recipientName'
}

exports.map = source => {
	return objectMapper(source, map)
}
