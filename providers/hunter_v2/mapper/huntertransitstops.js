// HUNTER V2

let objectMapper = require('object-mapper')

let map = {
	name: 'contact',
	stopReference: 'stopType',
	instructions: 'instructions'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.postalAddress = {
		address1: source.addressLine1,
		address2: source.addressLine2,
		country: source.country,
		postCode: source.postCode,
		state: source.state,
		suburb: source.suburbName
	}
	return destination
}
