// HUNTER V2

let objectMapper = require('object-mapper')
let _ = require('lodash')

let map = {
	BasePrice: 'netPrice',
	TaxPrice: 'taxPrice',
	ServiceType: 'serviceType',
	Title: 'serviceName',
	GrandPrice: 'grossPrice',
	Transit: 'etd'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	return destination
}
