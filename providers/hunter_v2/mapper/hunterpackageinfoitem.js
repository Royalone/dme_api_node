// HUNTER V2

let objectMapper = require('object-mapper')

let map = {
	length: {
		key: 'Length',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	width: {
		key: 'Width',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	height: {
		key: 'Height',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	weight: {
		key: 'Weight',
		transform: function (value) {
			return value.toFixed(2)
		}
	},
	quantity: 'Qty',
	// quantity: 'ItemRowChargeQty',
	description: 'Description',
	reference: 'Reference'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.ItemRowChargeQty = destination.Qty
	destination.QtyDecimal = destination.Qty
	return destination
}
