// HUNTER V2

let objectMapper = require('object-mapper')

let map = {
	postalAddress: [
		{
			key: 'suburbName',
			transform: function (value) {
				return value.suburb
			}
		},
		{
			key: 'state',
			transform: function (value) {
				return value.state
			}
		},
		{
			key: 'postCode',
			transform: function (value) {
				return value.postCode
			}
		}
	]
}

exports.map = source => {
	let destination = objectMapper(source, map)
	return destination
}
