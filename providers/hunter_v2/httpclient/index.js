// HUNTER V2

const unirest = require('unirest')
const logger = require('logger')
const axios = require('axios')

exports.createAuthHeaders = (accountKey) => {
	return {
		'Authorization': accountKey,
	}
}

exports.bookConsignment = (bookingRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_V2_BOOKING,
		method: 'post',
		data: bookingRequest
	}	

	logger.info('HUNTER_V2 BOOK Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			console.log('response', response)
			let responseData = Object.assign({}, response.data.Data)
			responseData.PdfLabels = responseData.PdfLabels.slice(0, 10) + "..."
			responseData.PdfConsignment = responseData.PdfConsignment.slice(0, 10) + "..."
			logger.info('HUNTER_V2 Booking Response: ' + JSON.stringify(responseData, null, 4))
			resolve(response.data)
		}).catch(error => {
			console.log('error', error)
			if( error.response )
				reject(error.response.data.errors)
			else 
				reject(error)
		})
	})
}

exports.updateConsignment = (shipmentRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_V2_BOOKING,
		data: shipmentRequest,
		method: 'put'
	}

	logger.info('HUNTER_V2 EditBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('HUNTER_V2 EditBook Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('HUNTER_V2 EditBook Failed: ' + JSON.stringify(error.response.data.errors, null, 4))
			reject(error.response.data.errors)
		})
	})
}

exports.cancelConsignment = (consignmentId, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		data: {
			UniqueId: consignmentId
		},
		method: 'delete'
	}

	logger.info('HUNTER_V2 CancelBook Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios.delete(process.env.SERVICE_FP_HUNTER_V2_BOOKING, params)
			.then(response => {
				logger.info('HUNTER_V2 CancelBook Success')
				resolve(response.data)
			}).catch(error => {
				console.log('HUNTER_V2 CancelBook Failed', error)
				logger.info('HUNTER_V2 CancelBook Failed')
				reject(error.response.data.errors)
			})
	})
}

exports.pricingConsignment = (pricingRequest, accountKey) => {
	const params = {
		headers: this.createAuthHeaders(accountKey),
		url: process.env.SERVICE_FP_HUNTER_V2_PRICING,
		method: 'post',
		data: pricingRequest
	}
	logger.info('HUNTER_V2 Pricing Request: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('HUNTER_V2 Pricing Success:' +  JSON.stringify(response.data, null, 4))
			resolve(response.data.Data.Rows)
		}).catch(error => {
			if (error.response) {
				logger.info('HUNTER_V2 Pricing Failure:' +  JSON.stringify(error.response.data.errors, null, 4))
				reject(error.response.data.errors)
			} else {
				logger.info('HUNTER_V2 Pricing Failure:' +  JSON.stringify(error, null, 4))
				reject(error)
			}
		})
	})
}

exports.getJobStatuses = (trackingNumber, accountKey) => {
	const params = {
		headers: this.createAuthHeaders(accountKey),
		url: "https://api.transvirtual.com.au/Api/ConsignmentStatus",
		method: 'post',
		data: {
			Number: trackingNumber
		}
	}
	logger.info('HUNTER_V2 Tracking Payload: ' + JSON.stringify(params))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('HUNTER_V2 Tracking Success:' +  JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			if (error.response) {
				logger.info('HUNTER_V2 Tracking Failure:' +  JSON.stringify(error.response.data.errors, null, 4))
				reject(error.response.data.errors)
			} else {
				logger.info('HUNTER_V2 Tracking Failure:' +  JSON.stringify(error, null, 4))
				reject(error)
			}
		})
	})
}

exports.fetchPod = (jobNumber, jobDate, customerCode, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	return new Promise(function (resolve, reject) {
		let request = {
			customerCode: customerCode,
			jobNumber: jobNumber,
			jobDate: jobDate
		}

		logger.info('HUNTER_V2 POD Payload: ' + JSON.stringify(request))

		unirest.get(process.env.SERVICE_FP_HUNTER_POD)
			.headers(headers)
			.strictSSL(false)
			.query(request)
			.then(response => {
				if (response.code >= 300) {
					logger.info('HUNTER_V2 POD Failed: ' +  JSON.stringify(response.body, null, 4))
					reject(response.body)
				} else { 
					let responseBody = JSON.parse(JSON.stringify(response.body))
					responseBody[0]["podImage"] = responseBody[0]["podImage"].slice(0, 10) + "..."

					logger.info('HUNTER_V2 POD Success: ' +  JSON.stringify(responseBody, null, 4))
					resolve(response.body) 
				}
			}).catch(error => {
				logger.info('HUNTER_V2 POD Failed: ' +  JSON.stringify(error, null, 4))
				reject(error)
			})
	})
}


exports.uploadPod = (consignmentId, consignmentNo, base64PodImage, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)
	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_V2_TRACKING,
		data: {
			UniqueId: consignmentId,
			ConsignmentNumber: consignmentNo,
			DateTime:destination.consolidate = moment().format('YYYY-MM-DD HH:mm'),
			Base64SignatureImage: base64PodImage
		},
		method: 'put'
	}

	logger.info('HUNTER_V2 UploadPOD Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('HUNTER_V2 UploadPOD Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('HUNTER_V2 UploadPOD Failed: ' + JSON.stringify(error.response.data.errors, null, 4))
			reject(error.response.data.errors)
		})
	})
}


exports.uploadLabel = (consignmentId, consignmentNo, base64LabelImage, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)
	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_HUNTER_V2_TRACKING,
		data: {
			UniqueId: consignmentId,
			ConsignmentNumber: consignmentNo,
			DateTime:destination.consolidate = moment().format('YYYY-MM-DD HH:mm'),
			Base64Image: base64LabelImage
		},
		method: 'put'
	}

	logger.info('HUNTER_V2 UploadLabel Payload: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('HUNTER_V2 UploadLabel Success: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			logger.info('HUNTER_V2 UploadLabel Failed: ' + JSON.stringify(error.response.data.errors, null, 4))
			reject(error.response.data.errors)
		})
	})
	
}