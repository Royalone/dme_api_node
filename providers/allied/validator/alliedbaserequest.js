exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountState || spAccountDetails.accountState === '') {
			validated = false
			throw new Error('Allied service provider account state cannot be empty in the request')
		}

		if (!spAccountDetails.accountKey || spAccountDetails.accountKey === '') {
			validated = false
			throw new Error('Service provider account key cannot be null or empty for star track service requests')
		}
	} catch (error) {
		reject({ errorMessage: error.message })
	}

	return validated
}
