const ServiceLevelEnum = [
	'C',
	'L',
	'R',
	'DS',
	'IC',
	'SD']

exports.validateRequest = (serviceLevel) => {
	let validated = true

	if (serviceLevel === null) {
		validated = false
		throw new Error('Service type cannot be null or empty for allied request. Allowed values are ' + ServiceLevelEnum.join())
	} else {
		if (ServiceLevelEnum.indexOf(serviceLevel.trim()) < 0) {
			validated = false
			throw new Error('Invalid service type value for allied request. Allowed values are ' + ServiceLevelEnum.join())
		}
	}

	return validated
}
