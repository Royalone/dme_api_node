
const alliedServiceTypeValidator = require('./alliedservicetype')

exports.validateRequest = (req, reject) => {
	const { serviceType } = req.body
	let validated = true
	if (!alliedServiceTypeValidator.validateRequest(serviceType)) { validated = false }
	return validated
}
