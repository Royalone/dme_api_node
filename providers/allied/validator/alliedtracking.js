exports.validateRequest = (req, reject) => {
	const { consignmentDetails } = req.body
	let validated = true

	for (const consignmentDetail of consignmentDetails) {
		if (!consignmentDetail.consignmentNumber || consignmentDetail.consignmentNumber === '') {
			validated = false
			throw new Error('Consignment number cannot be null or empty in Allied tracking request')
		} else if (!consignmentDetail.destinationPostcode || consignmentDetail.destinationPostcode === '') {
			validated = false
			throw new Error('Consignment destination code cannot be null or empty in Allied tracking request')
		}
	}

	return validated
}
