const soap = require('soap')
const logger = require('logger')

let alliedWsClient = null

exports.getAlliedWSClient = async () => {
	let wsdlLocation;

	if (process.env.ENV == 'dev') {
		// wsdlLocation = 'http://triton.alliedexpress.com.au:8080/ttws-ejb/TTWS?wsdl' // DEV
		wsdlLocation = 'http://localhost:3000/wsdls/allied_dev' // DEV
	} else {
		// wsdlLocation = 'https://neptune.alliedexpress.com.au:8443/ttws-ejb/TTWS?wsdl' // PROD
		wsdlLocation = 'http://localhost:3000/wsdls/allied_prod' // PROD
	}

	let client = await soap.createClientAsync(wsdlLocation)
	return client
}

exports.validateBooking = async (string1, job2) => {
	logger.info(`ALLIED [validateBooking] Request payload: string1=${string1}, job2=${job2}`)

	try {
		let result = await alliedWsClient.validateBookingAsync({ string1, job2 })

		if (result)
			result = result[0]['result']

		logger.info(`ALLIED [validateBooking] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		logger.info(`ALLIED [validateBooking] Failure: ${error}`)
		throw error.message
	}
}

exports.validateBookingExt = async (string1, job2) => {
	logger.info(`Calling endpoint service validateBookingExt - Request payload: string1=${string1}, job2=${job2}`)

	let job = null

	try {
		job = await alliedWsClient.validateBookingExtAsync({ string1, job2 })
	} catch (error) {
		throw error.message
	}
	return job
}

exports.dispatchPendingJobs = async (string1, job2) => {
	logger.info(`ALLIED [dispatchPendingJobs] Request payload: string1=${string1}, job2=${JSON.stringify(job2)}`)

	try {
		let result = await alliedWsClient.dispatchPendingJobsAsync({ string1, job2 })

		if (result)
			result = result[0]['result']['item']

		logger.info(`ALLIED [dispatchPendingJobs] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		logger.info(`ALLIED [dispatchPendingJobs] Failure: ${error}`)
		throw error.message
	}
}

exports.bookLocalCourierJob = async (string1, job2) => {
	logger.info(`Calling endpoint service bookLocalCourierJob - Request payload: string1=${string1}, job2=${job2}`)

	let job = null

	try {
		job = await alliedWsClient.bookLocalCourierJobAsync({ string1, job2 })
	} catch (error) {
		throw error.message
	}
	return job
}

exports.quoteLocalCourierJob = async (string1, job2) => {
	logger.info(`Calling endpoint service quoteLocalCourierJob - Request payload: string1=${string1}, job2=${job2}`)

	let job = null

	try {
		job = await alliedWsClient.quoteLocalCourierJobAsync({ string1, job2 })
	} catch (error) {
		throw error.message
	}
	return job
}

exports.savePendingJob = async (string1, job2) => {
	logger.info(`ALLIED [savePendingJob] Request payload: string1=${string1}, job2=${JSON.stringify(job2)}`)

	try {
		let result = await alliedWsClient.savePendingJobAsync({ string1, job2 })
		logger.info(`ALLIED [savePendingJob] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		logger.info(`ALLIED [savePendingJob] Failure: ${error}`)
		throw error.message
	}
}

exports.calculatePrice = async (string1, job2) => {
	logger.info(`ALLIED [calculatePriceAsync] Request payload: string1=${string1}, job2=${JSON.stringify(job2)}`)

	try {
		let result = await alliedWsClient.calculatePriceAsync({ string1, job2 })

		if (result)
			result = result[0]['result']

		logger.info(`ALLIED [calculatePriceAsync] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		logger.info(`ALLIED [calculatePriceAsync] Failure: ${error}`)
		throw error.message
	}
}

exports.cancelPendingJob = async (string1, int2) => {
	logger.info(`ALLIED [cancelPendingJobAsync] Request payload: string1=${string1}, int2=${int2}`)

	try {
		const result = await alliedWsClient.cancelPendingJobAsync({ string1, int2 })
		logger.info(`ALLIED [cancelPendingJobAsync] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		throw error.message
		logger.info(`ALLIED [cancelPendingJobAsync] Failure: ${error}`)
	}
}

exports.chargeJobs = async (string1, jobIds2) => {
	logger.info(`Calling endpoint service chargeJobs - Request payload: string1=${string1}, jobIds2=${jobIds2}`)
	try {
		await alliedWsClient.chargeJobsAsync({ string1, jobIds2 })
	} catch (error) {
		throw error.message
	}
}

exports.getLabel = async (string1, string2, string3, string4, string5, int6) => {
	logger.info(`Calling endpoint service getLabel - Request payload: string1=${string1}, string2=${string2}, string3=${string3}, string4=${string4}, string5=${string5}, int6=${int6}`)

	let label = null

	try {
		label = await alliedWsClient.getLabelAsync({ string1, string2, string3, string4, string5, int6 })
	} catch (error) {
		throw error.message
	}
	return label
}

exports.getJobDetails = async (string1, string2, string3, string4, string5) => {
	logger.info(`ALLIED [getJobDetails] Request payload: string1=${string1}, string2=${string2}, string3=${string3}, string4=${string4}, string5=${string5}`)

	try {
		let result = await alliedWsClient.getJobDetailsAsync({ string1, string2, string3, string4, string5 })

		if (result)
			result = result[0]['result']

		logger.info(`ALLIED [getJobDetailsAsync] Success: ${JSON.stringify(result)}`)
		return result
	} catch (error) {
		logger.info(`ALLIED [getJobDetailsAsync] Failure: ${error}`)
		throw error.message
	}
}

exports.fetchAccount = async (accountCode, accountKey, accountState) => {
	logger.info(`ALLIED [fetchAccount] accountCode: ${accountCode}, accountKey: ${accountKey}`)

	const client = await alliedWsClient.validateClientAsync({ accountKey })
	logger.info(`ALLIED [validateClientAsync] accountCode: ${accountCode}, accountKey: ${accountKey}, Client: ${JSON.stringify(client[0]['result'])}`)

	if (!client)
		throw new Error(`ALLIED [validateClientAsync] Client not found! accountCode: ${accountCode}, accountKey: ${accountKey}`)

	const result = await alliedWsClient.getAccountDefaultsAsync({ accountKey, accountCode, accountState, defaultShippingDivision: client.defaultShippingDivision })
	logger.info(`ALLIED [getAccountDefaultsAsync] accountCode: ${accountCode}, accountKey: ${accountKey}, Account: ${result}`)

	if (!result)
		throw new Error(`ALLIED [getAccountDefaultsAsync] Account not found! accountCode: ${accountCode}, accountKey: ${accountKey}`)
	else
		logger.info(`ALLIED [getAccountDefaultsAsync] accountCode: ${accountCode}, accountKey: ${accountKey}, Account: ${JSON.stringify(result[0]['result'])}`)
		return result[0]['result']
};

(async () => {
	alliedWsClient = await this.getAlliedWSClient()
	logger.info('ALLIED [Initialized] alliedWsClient: ' + alliedWsClient)
})()
