const logger = require('logger')
const moment = require('moment')

const alliedBookingRequestMapper = require('../mapper/alliedbookingrequest')
const alliedBookingResponseMapper = require('../mapper/alliedbookingresponse')

const alliedTransitStopRequestMapper = require('../mapper/alliedtransitstoprequest')
const alliedHttpClient = require('../httpclient')
const alliedBaseRequestValidator = require('../validator/alliedbaserequest')
const alliedBookingValidator = require('../validator/alliedbooking')


exports.bookConsignmentWithFreightProvider = (req) => {
	const self = this
	return new Promise(async (resolve, reject) => {
		logger.info(req.body)

		if (!alliedBaseRequestValidator.validateRequest(req, reject) || !alliedBookingValidator.validateRequest(req, reject)) {
			logger.info('ALLIED validation fail')
			return
		}
		const { accountKey } = req.body.spAccountDetails
		try {
			let validatedJob = await self.validateConsignmentBooking(req)
			let savedPendingJob = await alliedHttpClient.savePendingJob(accountKey, validatedJob)

			let jobIds = {jobIds: [validatedJob.jobNumber]}
			let dispatchedJob = await alliedHttpClient.dispatchPendingJobs(accountKey, jobIds)

			resolve(alliedBookingResponseMapper.map(dispatchedJob))
		} catch (error) {
			logger.error(`ALLIED BOOK Error: ${error}`)
			reject(error)
		}
	})
}

exports.validateConsignmentBooking = async (req) => {
	const { spAccountDetails } = req.body
	let job = await this.createBookingCourierJob(req)
	let validatedJob = await alliedHttpClient.validateBooking(spAccountDetails.accountKey, job)
	return validatedJob
}

exports.createBookingCourierJob = async (req) => {
	const { accountCode, accountKey, accountState } = req.body.spAccountDetails
	let account = await alliedHttpClient.fetchAccount(accountCode, accountKey, accountState)
	let job = alliedBookingRequestMapper.map(req.body)
	job.account = account
	job.readyDate = moment(job.readyDate, 'YYYY-MM-DD').format('YYYY-MM-DDTHH:mm:ss')
	job.referenceNumbers = ['AAA']
	// job.referenceNumbers.push(req.body.referenceNumber)
	job.jobStops = this.createJobStops(req)
	return job
}

exports.createJobStops = (req) => {
	let jobStops = []
	let pickupStop = alliedTransitStopRequestMapper.map(req.body.pickupAddress)
	let dropStop = alliedTransitStopRequestMapper.map(req.body.dropAddress)

	pickupStop.stopNumber = 1
	dropStop.stopNumber = 2

	pickupStop.stopType = 'P'
	dropStop.stopType = 'D'

	jobStops.push(pickupStop)
	jobStops.push(dropStop)
	return jobStops
}

exports.cancelConsignment = (req) => {
	return new Promise(async (resolve, reject) => {
		if (!alliedBaseRequestValidator.validateRequest(req, reject)) { return }

		try {
			let bookingCancelResults = []
			const { consignmentDetails } = req.body

			const { accountCode, accountKey, accountState } = req.body.spAccountDetails
			let account = await alliedHttpClient.fetchAccount(accountCode, accountKey, accountState)

			for (const consignmentDetail of consignmentDetails) {
				const { destinationPostcode, consignmentNumber, consignmentReferenceNumber } = consignmentDetail
				let jobResponse = await alliedHttpClient.getJobDetails(accountKey, account.shippingDivision, consignmentReferenceNumber, consignmentNumber, destinationPostcode)
				await alliedHttpClient.cancelPendingJob(accountKey, jobResponse.jobNumber)

				let consignmentBookingCancelResult = {}
				consignmentBookingCancelResult.consignmentDetails = consignmentDetails
				consignmentBookingCancelResult.status = true
				bookingCancelResults.push(consignmentBookingCancelResult)
			}

			let consignmentBookingCancelResponse = {}

			consignmentBookingCancelResponse.bookingCancelResults = bookingCancelResults
			resolve(consignmentBookingCancelResponse)
		} catch (error) {
			reject(error)
		}
	})
}
