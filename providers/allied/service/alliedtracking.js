const logger = require('logger')

const alliedHttpClient = require('../httpclient')
const alliedBaseRequestValidator = require('../validator/alliedbaserequest')
const alliedTransitStopResponseMapper = require('../mapper/alliedtransitstopresponse')
const alliedCubedPackageInfoItemMapper = require('../mapper/alliedcubedpackageinfoitem')
const alliedPriceConsignmentPriceMapper = require('../mapper/alliedpriceconsignmentprice')
/**
 *
 * @param {*ConsignmentTrackingRequest} req
 */
exports.getTrackingDetail = (req) => {
	const self = this
	return new Promise(async (resolve, reject) => {
		if (!alliedBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		try {
			const { consignmentDetails } = req.body
			let trackDetailsResponseList = []

			const { accountCode, accountKey, accountState } = req.body.spAccountDetails
			let account = await alliedHttpClient.fetchAccount(accountCode, accountKey, accountState)

			for (let consignmentDetail of consignmentDetails) {
				const { de_to_address_postcode, consignmentNumber } = consignmentDetail
				let jobResponse = await alliedHttpClient.getJobDetails(accountKey, account.shippingDivision, consignmentNumber, "", de_to_address_postcode)
				let trackDetailResponse = self.createConsignmentTrackDetails(jobResponse, jobResponse.jobNumber)
				trackDetailsResponseList.push(trackDetailResponse)
			}

			let response = {}
			response.consignmentTrackDetails = trackDetailsResponseList
			logger.info(`ALLIED [getTrackingDetail] Success: ${response}`)
			resolve(response)
		} catch (error) {
			logger.info(`ALLIED [getTrackingDetail] Failure: ${error}`)
			reject(error)
		}
	})
}

exports.createConsignmentTrackDetails = (jobResponse, consignmentNumber) => {
	let transitStopList = []
	for (let jobStop of jobResponse.jobStops) {
		transitStopList.push(alliedTransitStopResponseMapper.map(jobStop))
	}

	let packageInfoList = []
	let items = jobResponse.items

	if (!Array.isArray(items))
		items = [jobResponse.items]

	for (let packageInfo of items)
		packageInfoList.push(alliedCubedPackageInfoItemMapper.map(packageInfo))

	let consignmentPrice = alliedPriceConsignmentPriceMapper.map(jobResponse.price)
	let consignmentStatuses = this.createConsignmentStatus(jobResponse)
	let pods = this.createPods(jobResponse)
	let trackDetailResponse = {}
	trackDetailResponse.consignmentStops = transitStopList
	trackDetailResponse.packageDetails = packageInfoList
	trackDetailResponse.consignmentNumber = jobResponse.docketNumber
	trackDetailResponse.consignmentStatuses = consignmentStatuses
	trackDetailResponse.pods = pods
	trackDetailResponse.consignmentPrice = consignmentPrice
	trackDetailResponse.scheduledPickupDate = jobResponse.scheduledPickupDate
	trackDetailResponse.scheduledDeliveryDate = jobResponse.scheduledDeliveryDate
	trackDetailResponse.pickupDate = jobResponse.pickupDate
	trackDetailResponse.deliveryDate = jobResponse.deliveryDate
	trackDetailResponse.totalItems = jobResponse.totalItems
	trackDetailResponse.totalVolume = jobResponse.totalVolume
	trackDetailResponse.totalWeight = jobResponse.totalWeight
	trackDetailResponse.consignmentReferenceNumber = jobResponse.referenceNumbers
	trackDetailResponse.jobNumber = jobResponse.jobNumber
	trackDetailResponse.jobStatus = jobResponse.jobStatus

	return trackDetailResponse
}

exports.createConsignmentStatus = (jobResponse) => {
	let consignmentStatuses = []
	if ( !jobResponse.scannings) return null;

	let scannings = jobResponse.scannings

	if (!Array.isArray(scannings))
		scannings = [jobResponse.scannings]

	for (const scan of scannings) {
		if (!scan) continue
		let consignmentStatuse = {}
		consignmentStatuse.status = scan.batchID
		consignmentStatuse.statusDescription = scan.consignmentNote
		consignmentStatuse.statusUpdate = scan.scanDate
		consignmentStatuse.userId = scan.driverNum
		consignmentStatuses.push(consignmentStatuse)
	}
	return consignmentStatuses
}

exports.createPods = (jobResponse) => {
	if ( !jobResponse.pods) return null;

	let consignmentresults = []
	let pods =jobResponse.pods

	if (!Array.isArray(pods))
		pods = [jobResponse.pods]

	for (const pod of pods) {
		if (!pod) continue
		let consignmentresult = {}
		consignmentresult.consignmentNote = pod.consignmentNote
		consignmentresult.podData = pod.podData
		consignmentresults.push(consignmentresult)
	}

	return consignmentresults
}
