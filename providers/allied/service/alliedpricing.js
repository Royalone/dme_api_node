const logger = require('logger')
const moment = require('moment')

const alliedBookingRequestMapper = require('../mapper/alliedbookingrequest')
const alliedHttpClient = require('../httpclient')
const alliedBaseRequestValidator = require('../validator/alliedbaserequest')
const alliedPricingValidator = require('../validator/alliedpricing')

const alliedTransitStopRequestMapper = require('../mapper/alliedtransitstoprequest')
const alliedJobPricePricingMapper = require('../mapper/alliedjobpricepricing')


exports.getPriceDetail = (req) => {
	const self = this
	return new Promise(async (resolve, reject) => {
		logger.info(`Allied Pricing get started!`)
		if (!alliedBaseRequestValidator.validateRequest(req, reject) || !alliedPricingValidator.validateRequest(req, reject)) {
			return
		}

		const { accountKey } = req.body.spAccountDetails
		try {
			let job = await self.createCalculatePriceJob(req)
			let jobPrice = await alliedHttpClient.calculatePrice(accountKey, job)

			resolve(alliedJobPricePricingMapper.map(jobPrice))
		} catch (error) {
			logger.info(`Allied Pricing Error: ${error}`)
			reject(error)
		}
	})
}

exports.createJobStops = (req) => {
	let jobStops = []
	let pickupStop = alliedTransitStopRequestMapper.map(req.body.pickupAddress)
	let dropStop = alliedTransitStopRequestMapper.map(req.body.dropAddress)

	pickupStop.stopNumber = 1
	dropStop.stopNumber = 2

	pickupStop.stopType = 'P'
	dropStop.stopType = 'D'

	jobStops.push(pickupStop)
	jobStops.push(dropStop)
	return jobStops
}

exports.createCalculatePriceJob = async (req) => {
	logger.info(`Allied Pricing create price job`)
	const { accountCode, accountKey, accountState } = req.body.spAccountDetails
	logger.info(`Allied Pricing account details - accountCode: ${accountCode}, accountKey: ${accountKey}, accountState: ${accountState}`)
	let account = null;

	try {
		account = await alliedHttpClient.fetchAccount(accountCode, accountKey, accountState)
		logger.info(`Allied account: ${account}`)
	} catch (error) {
		logger.info(`Allied Fetch account error: ${error}`)
		throw('Allied Fetch account get failed!')
	}

	let job = alliedBookingRequestMapper.map(req.body)
	job.account = account
	// job.referenceNumbers = []
	// job.referenceNumbers.push(req.body.referenceNumber)
	job.readyDate = moment().format('YYYY-MM-DDTHH:mm:ss')
	job.jobStops = this.createJobStops(req)

	logger.info(`Allied Pricing createCalculatePriceJob - result: ${job}`)
	return job
}
