/**
 *
 * @param {*} req
 */

const alliedHttpClient = require('../httpclient')
const alliedBaseRequestValidator = require('../validator/alliedbaserequest')

exports.getFreightProviderLabel = (req) => {
	return new Promise(async function (resolve, reject) {
		if (!alliedBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		try {
			const { accountCode, accountKey, accountState } = req.body.spAccountDetails
			let account = await alliedHttpClient.fetchAccount(accountCode, accountKey, accountState)
			const shippingDiv = account.shippingDivision

			const { consignmentNumber, referenceNumber, destinationPostcode, labelType } = req.body
			let encodedPdfData = await alliedHttpClient.getLabel(accountKey, shippingDiv, consignmentNumber, referenceNumber, destinationPostcode, parseInt(labelType))

			let response = {}
			response.encodedPdfData = encodedPdfData
			resolve(response)
		} catch (error) {
			reject(error)
		}
	})
}

/**
 *
 * @param {*} req
 */
exports.createFreightProviderLabel = (req) => {
}
