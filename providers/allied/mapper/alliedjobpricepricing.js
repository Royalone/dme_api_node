const alliedPriceConsignmentPriceMapper = require('./alliedpriceconsignmentprice')
let objectMapper = require('object-mapper')

let map = {
	surcharges: {
		key: 'surcharges',
		transform: function (items) {
			let newItems = []
			for (const item of items) {
				newItems.push(alliedPriceConsignmentPriceMapper.map(item))
			}
			return newItems
		}
	},
	totalCharge: 'totalPrice', // Net price + Surcharge price
	jobCharge: 'netPrice', // Net price
}

exports.map = source => {
	return objectMapper(source, map)
}
