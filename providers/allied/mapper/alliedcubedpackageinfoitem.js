let objectMapper = require('object-mapper')

let map = {
	dangerous: 'dangerous',
	height: 'height',
	width: 'width',
	weight: 'weight',
	length: 'length',
	volume: 'volume',
	barcode: 'barcode'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	return destination
}
