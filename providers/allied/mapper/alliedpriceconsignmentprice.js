let objectMapper = require('object-mapper')

let map = {
	chargeCode: 'name',
	description: 'description',
	netValue: 'amount',
	quantity: 'qty',
	chargeQuantity: 'chargeQuantity',
	cubicFactor: 'chargeQuantity',
	discountRate: 'discountRate',
	grossPrice: 'grossPrice',
	netPrice: 'netPrice'
}

exports.map = source => {
	return objectMapper(source, map)
}
