
const alliedPriceConsignmentPriceMapper = require('./alliedpriceconsignmentprice')
let objectMapper = require('object-mapper')

let map = {
	// price: {
	// 	key: 'price',
	// 	transform: function (items) {
	// 		let newItems = []
	// 		for (const item of items) {
	// 			newItems.push(alliedPriceConsignmentPriceMapper.map(item))
	// 		}
	// 		return newItems
	// 	}
	// },
	// referenceNumbers: {
	// 	key: 'consignmentReferenceNumber',
	// 	transform: function (value) {
	// 		return value.join()
	// 	}
	// },
	bookedDate: 'bookingDateTime',
	jobStatus: 'status',
	jobNumber: 'jobNumber',
	docketNumber: 'consignmentNumber'
}

exports.map = source => {
	return objectMapper(source, map)
}
