let objectMapper = require('object-mapper')

let map = {
	dangerous: 'dangerous',
	height: 'height',
	width: 'width',
	weight: 'weight',
	length: 'length',
	volume: 'volume',
	quantity: 'itemCount'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	// destination.volume = (source.height / 100) * (source.width / 100) * (source.length / 100)
	return destination
}
