let objectMapper = require('object-mapper')

let map = {
	contact: 'contact',
	companyName: 'companyName',
	emailAddress: 'emailAddress',
	phoneNumber: 'phoneNumber',
	stopType: 'stopType',
	postalAddress: {
		key: 'geographicAddress',
		transform: function (value) {
			return {
				postCode: value.postCode,
				country: value.country,
				state: value.state,
				address1: value.address1,
				address2: value.address2,
				suburb: value.suburb,
				sortCode: value.sortCode
			}
		}
	}
}

exports.map = source => {
	return objectMapper(source, map)
}
