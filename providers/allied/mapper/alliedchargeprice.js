
let objectMapper = require('object-mapper')

let map = {
	chargeCode: 'rateCode',
	description: 'reason',
	netValue: 'netPrice',
	quantity: 'chargeQuantity'
}

exports.map = source => {
	return objectMapper(source, map)
}
