let objectMapper = require('object-mapper')
let alliedPackageInfoItemMapper = require('./alliedpackageinfoitem')
let _ = require('lodash')

let map = {
	items: [
		{
			key: 'itemCount',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity
				})
			}
		},
		{
			key: 'volume',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * parseFloat(item.volume).toFixed(5)
				})
			}
		},
		{
			key: 'weight',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.weight
				})
			}
		},
		{
			key: 'items',
			transform: function (items) {
				let newItems = []
				for (const item of items) {
					newItems.push(alliedPackageInfoItemMapper.map(item))
				}

				return newItems
			}
		}
	],
	readyDate: 'readyDate',
	bookedBy: 'bookedBy',
	docketNumber: 'docketNumber',
	serviceType: 'serviceLevel'
}

exports.map = source => {
	return objectMapper(source, map)
}
