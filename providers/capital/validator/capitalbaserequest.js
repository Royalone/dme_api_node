exports.validateRequest = (req, reject) => {
	const { spAccountDetails } = req.body
	let validated = true

	try {
		if (!spAccountDetails.accountKey || spAccountDetails.accountKey === '') {
			validated = false
			throw new Error('Account key cannot be null or empty for Capital service provider. Please provide valid shared key')
		}

		if (!spAccountDetails.accountUsername || spAccountDetails.accountUsername === '') {
			validated = false
			throw new Error('Username cannot be null or empty for Capital service provider. Please provide valid user name')
		}
	} catch (error) {
		reject(error.message)
	}

	return validated
}
