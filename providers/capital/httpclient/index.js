const axios = require('axios')
const logger = require('logger')
exports.createAuthHeaders = (accountKey) => {
	return {
		'Content-Type': 'application/json',
		Accept: 'application/json',
		Authorization: this.createAuthorizationApiKey(accountKey)
	}
}

exports.createAuthorizationApiKey = (accountKey) => {
	return 'Basic ' + accountKey
}

exports.bookConsignment = (bookingRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_CAPITAL_BOOKING,
		data: bookingRequest,
		method: 'post'
	}

	logger.info('CAPITAL Booking Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			let responseData = Object.assign({}, response.data)

			responseData.Label = responseData.Label.slice(0, 10) + "..."
			logger.info('CAPITAL Booking Response: ' + JSON.stringify(responseData, null, 4))
			resolve(response.data)
		}).catch(error => {
			if( error.response )
				reject(error.response.data.errors)
			else 
				reject(error)
		})
	})
}

exports.calculatePrice = (pricingRequest, accountKey) => {
	const headers = this.createAuthHeaders(accountKey)

	const params = {
		headers: headers,
		url: process.env.SERVICE_FP_CAPITAL_PRICING,
		data: pricingRequest,
		method: 'post'
	}

	logger.info('CAPITAL Pricing Params: ' + JSON.stringify(params, null, 4))

	return new Promise(function (resolve, reject) {
		axios(params).then(response => {
			logger.info('CAPITAL Pricing Response: ' + JSON.stringify(response.data, null, 4))
			resolve(response.data)
		}).catch(error => {
			if( error.response )
				reject(error.response.data.errors)
			else 
				reject(error)
		})
	})
}