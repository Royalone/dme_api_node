const capitalBookingRequestMapper = require('../mapper/capitalbookingrequest')
const capitalBookingResponseMapper = require('../mapper/capitalbookingresponse')
const capitalHttpClient = require('../httpclient')
const capitalBaseRequestValidator = require('../validator/capitalbaserequest')

/**
 *
 * @param {*} req
 */
exports.bookConsignmentWithFreightProvider = (req) => {
	return new Promise(function (resolve, reject) {
		if (!capitalBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const request = capitalBookingRequestMapper.map(req.body)
		capitalHttpClient.bookConsignment(request)
			.then(response => {
				resolve(capitalBookingResponseMapper.map(response))
			}).catch(error => {
				reject(error)
			})
	})
}
