const capitalPricingRequestMapper = require('../mapper/capitalpricingrequest')
const capitalPricingResponseMapper = require('../mapper/capitalpricingresponse')
const capitalHttpClient = require('../httpclient')
const capitalBaseRequestValidator = require('../validator/capitalbaserequest')

/**
 *
 * @param {*} req
 */
exports.getPriceDetail = (req) => {
	return new Promise(function (resolve, reject) {
		if (!capitalBaseRequestValidator.validateRequest(req, reject)) {
			return
		}

		const request = capitalPricingRequestMapper.map(req.body)
		capitalHttpClient.calculatePrice(request)
			.then(response => {
				resolve(capitalPricingResponseMapper.map(response))
			}).catch(error => {
				reject(error)
			})
	})
}
