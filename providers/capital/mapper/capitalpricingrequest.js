const capitalPackageinfoItemMapper = require('./capitalpackageinfoitem')
let objectMapper = require('object-mapper')
let _ = require('lodash')

let map = {
	pickupAddress: [
		{
			key: 'FromDetail1',
			transform: function (value) {
				return value.postalAddress.address1
			}
		},
		{
			key: 'FromDetail2',
			transform: function (value) {
				return ''
			}
		},
		{
			key: 'FromDetail3',
			transform: function (value) {
				return value.postalAddress.address2
			}
		},

		{
			key: 'FromSuburb',
			transform: function (value) {
				return value.postalAddress.suburb
			}
		},
		{
			key: 'FromDetail4',
			transform: function (value) {
				return value.postalAddress.state
			}
		},
		{
			key: 'FromDetail5',
			transform: function (value) {
				return value.postalAddress.country
			}
		},
		{
			key: 'FromPostcode',
			transform: function (value) {
				return value.postalAddress.postCode
			}
		},
		{
			key: 'BookingContactInformation',
			transform: function (value) {
				return {
					Name: value.contact,
					PhoneNumbers: [value.phoneNumber]
				}
			}
		}
	],
	dropAddress: [
		{
			key: 'ToDetail1',
			transform: function (value) {
				return value.postalAddress.address1
			}
		},
		{
			key: 'ToDetail2',
			transform: function (value) {
				return ''
			}
		},
		{
			key: 'ToDetail3',
			transform: function (value) {
				return value.postalAddress.address2
			}
		},
		{
			key: 'ToSuburb',
			transform: function (value) {
				return value.postalAddress.suburb
			}
		},
		{
			key: 'ToDetail4',
			transform: function (value) {
				return value.postalAddress.state
			}
		},
		{
			key: 'ToDetail5',
			transform: function (value) {
				return value.postalAddress.country
			}
		},
		{
			key: 'ToPostcode',
			transform: function (value) {
				return value.postalAddress.postCode
			}
		}
	],
	spAccountDetails: [
		{
			key: 'UserCredentials',
			transform: function (value) {
				return {
					Username: value.accountUsername,
					SharedKey: value.accountKey
				}
			}
		},
		{
			key: 'AccountCode',
			transform: function (value) {
				return value.accountCode
			}
		}
	],
	serviceType: 'ServiceCode',
	referenceNumber: 'Reference1',
	items: [
		{
			key: 'BookingItems',
			transform: function (items) {
				let newItems = []
				for (let item of items) {
					newItems.push(capitalPackageinfoItemMapper.map(item))
				}
				return newItems
			}
		},
		{
			key: 'TotalItems',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity
				})
			}
		},
		{
			key: 'TotalVolume',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.volume
				})
			}
		},
		{
			key: 'TotalWeight',
			transform: function (items) {
				return _.sumBy(items, function (item) {
					return item.quantity * item.weight
				})
			}
		}
	]
}

exports.map = source => {
	const accountStates = ['VIC', 'NSW', 'QLD', 'WA', 'SA']
	let destination = objectMapper(source, map)
	destination.SpecialInstructions = source.pickupAddress.instruction + ' ' + source.dropAddress.instruction
	destination.RequestType = 1

	const state = accountStates.indexOf(source.spAccountDetails.accountState)

	if (state < 0) { destination.State = 0 } else { destination.State = state + 1 }

	destination.Reference2 = ''
	destination.Caller = 'DME'
	destination.ExtraPuInformation = 'Please collect from warehouse'
	destination.ExtraDelInformation = 'Please collect from warehouse'
	// destination.ServiceCodes = ["SC","EC","VC","SSW","ESW","VSW","SWR","EWR","VWR"];
	return destination
}
