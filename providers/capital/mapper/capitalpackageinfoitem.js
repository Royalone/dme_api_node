let objectMapper = require('object-mapper')

let map = {
	length: 'Length',
	width: 'Width',
	height: 'Height',
	weight: 'Weight',
	barcode: 'Barcode',
	quantity: 'Quantity',
	volume: 'Volume',
	description: 'Description'
}

exports.map = source => {
	return objectMapper(source, map)
}
