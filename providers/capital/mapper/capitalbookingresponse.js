
let objectMapper = require('object-mapper')

let map = {
	JobNumber: 'consignmentNumber',
	Reference1: 'consignmentReferenceNumber',
	Reference2: 'consignmentReferenceNumber2',
	StatusCode: 'status',
	State: 'state',
	StatusDescription: 'statusDescription',
	Label: 'Label'
}

exports.map = source => {
	let destination = objectMapper(source, map)
	destination.price = {
		netPrice: source.JobTotalPrice,
		totalTaxes: source.Gst,
		jobPriceExGst: source.JobPriceExGst
	}
	return destination
}
